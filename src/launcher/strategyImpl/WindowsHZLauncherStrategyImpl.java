package launcher.strategyImpl;

import launcher.HorizonsLauncherStrategy;
import launcher.HZCharacter;
import launcher.Configuration;
import launcher.patcher.PatcherWindow;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Oceth
 */
public class WindowsHZLauncherStrategyImpl implements HorizonsLauncherStrategy {
	public void startHorizons(HZCharacter hzChar, boolean fullScan, JFrame launcherWindow) {
        if (new PatcherWindow(launcherWindow).runPatch(fullScan, hzChar.getShardName())) {
            File horizonsExe = Configuration.detectExecutable();
            if(horizonsExe == null || !horizonsExe.exists() || !horizonsExe.isFile())
            {
                JOptionPane.showMessageDialog(launcherWindow, "No game exe in \"" + Configuration.getConfig().getHorizonsPath() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            List<String> cmdl = new ArrayList<String>();
            cmdl.add(horizonsExe.getAbsolutePath());
            cmdl.add("weblaunch_standard");
            cmdl.add(Configuration.getConfig().getHzAccount().getUsername());
            cmdl.add(Configuration.getConfig().getHzAccount().getPassword());
            cmdl.add(hzChar.getShardName());
            cmdl.add(""+hzChar.getBiote());
            cmdl.add("0");
            cmdl.add(hzChar.getIP());
            cmdl.add(Configuration.getConfig().getHzAccount().getPublicKey());
            cmdl.add(hzChar.getCharacterName().split(" ")[0]);
            String[] cmd = (String[]) cmdl.toArray(new String[cmdl.size()]);

            try {
                Process p = Runtime.getRuntime().exec(cmd, null, new File(Configuration.getConfig().getHorizonsPath()));
                Configuration.getConfig().setLastChar(hzChar);
                Configuration.saveConfig();
                System.exit(0);
            }
            catch (IOException e) {
	    	e.printStackTrace();
                JOptionPane.showMessageDialog(launcherWindow, "Couldn't launch "+horizonsExe.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
	}
}

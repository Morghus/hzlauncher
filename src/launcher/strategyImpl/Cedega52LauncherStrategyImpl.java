package launcher.strategyImpl;

import launcher.Configuration;
import launcher.HZCharacter;
import launcher.HorizonsLauncherStrategy;
import launcher.linuxConfig.Cedega52Config;
import launcher.linuxConfig.CedegaGameIcon;
import launcher.patcher.PatcherWindow;
import launcher.util.CmdRunner;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.SubnodeConfiguration;

/**
 * @author Oceth
 */
public class Cedega52LauncherStrategyImpl implements HorizonsLauncherStrategy {
	public void startHorizons(HZCharacter hzChar, boolean fullScan, JFrame launcherWindow) {
		if (new PatcherWindow(launcherWindow).runPatch(fullScan, hzChar.getShardName())) {
            File horizonsExe = Configuration.detectExecutable();
            Cedega52Config ccfg = (Cedega52Config) Configuration.getConfig().getLinuxConfig();
            if(horizonsExe == null || !horizonsExe.exists() || !horizonsExe.isFile())
            {
                JOptionPane.showMessageDialog(launcherWindow, "No game exe in \"" + Configuration.getConfig().getHorizonsPath() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            List<String> cmdl = new ArrayList<String>();
            cmdl.add(ccfg.getCedegaPath());

            if(!ccfg.getWinVer().equals("Default"))
            {
                cmdl.add("-winver");
                cmdl.add(ccfg.getWinVer());
            }
            if(!ccfg.getDebugMsgs().trim().equals(""))
            {
                cmdl.add("-debugmsg");
                cmdl.add(ccfg.getDebugMsgs().trim());
            }
            if(!ccfg.getGddbConfig().equals("Default"))
            {
                cmdl.add("-gddb");
                cmdl.add(ccfg.getGddbConfig());
            }
            if(!ccfg.getGame().equals(new CedegaGameIcon()))
            {
                cmdl.add("-run");
                cmdl.add(ccfg.getGame().getGameName());
                cmdl.add(ccfg.getGame().getIconName());
            }
            else
            {
                cmdl.add(horizonsExe.getAbsolutePath());
            }

            StringBuilder cmdbuf = new StringBuilder();

            addArg(cmdl,cmdbuf,"weblaunch_standard");
            addArg(cmdl,cmdbuf,Configuration.getConfig().getHzAccount().getUsername());
            addArg(cmdl,cmdbuf,Configuration.getConfig().getHzAccount().getPassword());
            addArg(cmdl,cmdbuf,hzChar.getShardName());
            addArg(cmdl,cmdbuf,""+hzChar.getBiote());
            addArg(cmdl,cmdbuf,"0");
            addArg(cmdl,cmdbuf,hzChar.getIP());
            addArg(cmdl,cmdbuf,Configuration.getConfig().getHzAccount().getPublicKey());
            addArg(cmdl,cmdbuf,hzChar.getCharacterName().split(" ")[0]);

            File iniFileBack = null;
            File iniFile = null;
            if(ccfg.isWriteIniFile())
            {
                File cedegaPath = new File(System.getProperty("user.home"),".cedega");
                File gamePath = new File(cedegaPath, ccfg.getGame().getGameName());
                iniFile = new File(gamePath,"games.ini");
                try {
                    HierarchicalINIConfiguration cfg = new HierarchicalINIConfiguration(iniFile);
                    SubnodeConfiguration section = cfg.getSection(ccfg.getGame().getIconName());
                    iniFileBack = new File(gamePath,"games.ini.prehzl");
                    if(!iniFileBack.exists())
                    {
                        iniFile.renameTo(iniFileBack);
                    }
                    section.setProperty("commandlineargs",cmdbuf.toString());
                    cfg.save();
                } catch (ConfigurationException e) {
                    e.printStackTrace();
                }
            }

            String[] cmd = (String[]) cmdl.toArray(new String[cmdl.size()]);

			try {
				CmdRunner cmdr = new CmdRunner(cmd, null, new File(Configuration.getConfig().getHorizonsPath()),Configuration.getConfig().isEnableDebugConsole());
				cmdr.launcher = launcherWindow;
				cmdr.run();
				Configuration.getConfig().setLastChar(hzChar);
				Configuration.saveConfig();
				System.exit(0);
			}
			catch (IOException e) {
                JOptionPane.showMessageDialog(launcherWindow, "Couldn't launch "+horizonsExe.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

    private static void addArg(List<String> cmdline, StringBuilder buf, String arg)
    {
        cmdline.add(arg);
        if(buf.length() > 0) buf.append(" ");
        buf.append(arg);
    }
}

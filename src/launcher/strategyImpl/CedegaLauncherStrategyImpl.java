package launcher.strategyImpl;

import launcher.HorizonsLauncherStrategy;
import launcher.HZCharacter;
import launcher.Configuration;
import launcher.linuxConfig.CedegaConfig;
import launcher.util.CmdRunner;
import launcher.patcher.PatcherWindow;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

/**
 * @author Oceth
 */
public class CedegaLauncherStrategyImpl implements HorizonsLauncherStrategy {
	public void startHorizons(HZCharacter hzChar, boolean fullScan, JFrame launcherWindow) {
		if (new PatcherWindow(launcherWindow).runPatch(fullScan, hzChar.getShardName())) {
            File horizonsExe = Configuration.detectExecutable();
            CedegaConfig ccfg = (CedegaConfig) Configuration.getConfig().getLinuxConfig();
            if(horizonsExe == null || !horizonsExe.exists() || !horizonsExe.isFile())
            {
                JOptionPane.showMessageDialog(launcherWindow, "No game exe in \"" + Configuration.getConfig().getHorizonsPath() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            List<String> cmdl = new ArrayList<String>();
            cmdl.add(ccfg.getCedegaPath());

            if(!ccfg.getCedegaConfig().equals("Default"))
            {
                cmdl.add("-config");
                cmdl.add(ccfg.getCedegaConfig());
            }
            if(!ccfg.getWinVer().equals("Default"))
            {
                cmdl.add("-winver");
                cmdl.add(ccfg.getWinVer());
            }
            if(!ccfg.getDebugMsgs().trim().equals(""))
            {
                cmdl.add("-debugmsg");
                cmdl.add(ccfg.getDebugMsgs().trim());
            }
            if(!ccfg.getGame().equals("Default"))
            {
                cmdl.add("-install");
                cmdl.add(ccfg.getGame());
            }
            cmdl.add(horizonsExe.getAbsolutePath());
            cmdl.add("weblaunch_standard");
            cmdl.add(Configuration.getConfig().getHzAccount().getUsername());
            cmdl.add(Configuration.getConfig().getHzAccount().getPassword());
            cmdl.add(hzChar.getShardName());
            cmdl.add(""+hzChar.getBiote());
            cmdl.add("0");
            cmdl.add(hzChar.getIP());
            cmdl.add(Configuration.getConfig().getHzAccount().getPublicKey());
            cmdl.add(hzChar.getCharacterName().split(" ")[0]);

            String[] cmd = (String[]) cmdl.toArray(new String[cmdl.size()]);

			try {
				CmdRunner cmdr = new CmdRunner(cmd, null, new File(Configuration.getConfig().getHorizonsPath()),Configuration.getConfig().isEnableDebugConsole());
				cmdr.launcher = launcherWindow;
				cmdr.run();
				Configuration.getConfig().setLastChar(hzChar);
				Configuration.saveConfig();
				System.exit(0);
			}
			catch (IOException e) {
                JOptionPane.showMessageDialog(launcherWindow, "Couldn't launch "+horizonsExe.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}

package launcher.strategyImpl;

import launcher.Configuration;
import launcher.HZCharacter;
import launcher.HorizonsLauncherStrategy;
import launcher.patcher.PatcherWindow;
import launcher.util.CmdRunner;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

/**
 * @author Oceth
 */
public class LinuxHZLauncherStrategyImpl implements HorizonsLauncherStrategy {
	public void startHorizons(HZCharacter hzChar, boolean fullScan, JFrame launcherWindow) {
		if (new PatcherWindow(launcherWindow).runPatch(fullScan, hzChar.getShardName())) {
            File horizonsExe = Configuration.detectExecutable();
            if(horizonsExe == null || !horizonsExe.exists() || !horizonsExe.isFile())
            {
                JOptionPane.showMessageDialog(launcherWindow, "No game exe in \"" + Configuration.getConfig().getHorizonsPath() + "\"", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            String[] cmd = new String[]{"wine",
                    horizonsExe.getAbsolutePath(),
                    "weblaunch_standard",
                    Configuration.getConfig().getHzAccount().getUsername(),
                    Configuration.getConfig().getHzAccount().getPassword(),
                    hzChar.getShardName(),
                    ""+hzChar.getBiote(),
                    "0",
                    hzChar.getIP(),
                    Configuration.getConfig().getHzAccount().getPublicKey(),
                    hzChar.getCharacterName().split(" ")[0]};

			try {
				CmdRunner cmdr = new CmdRunner(cmd, null, new File(Configuration.getConfig().getHorizonsPath()),true);
				cmdr.launcher = launcherWindow;
				cmdr.run();
				Configuration.getConfig().setLastChar(hzChar);
				Configuration.saveConfig();
				System.exit(0);
			}
			catch (IOException e) {
                JOptionPane.showMessageDialog(launcherWindow, "Couldn't launch "+horizonsExe.getAbsolutePath(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}

package launcher.charactersUpdate;
import java.util.Observable;
import java.util.Observer;

import launcher.Configuration;
import launcher.HZCharacter;
import launcher.HorizonsService;
/*
 * Created on 25.06.2004
 * $Id: GetShardsThread.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */

/**
 * @author unknown
 */
public class GetShardsThread extends Observable implements Observer, Runnable {

	/**
	 * 
	 */
	Configuration config;
	String shardName;
	boolean getHZCharacter;
	
	/**
	 * @return Returns the getHZCharacter.
	 */
	public boolean isGetHZCharacter() {
		return getHZCharacter;
	}
	/**
	 * @param getHZCharacter The getHZCharacter to set.
	 */
	public void setGetHZCharacter(boolean getHZCharacter) {
		this.getHZCharacter = getHZCharacter;
	}
	/**
	 * @return Returns the shardName.
	 */
	public String getShardName() {
		return shardName;
	}
	/**
	 * @param shardName The shardName to set.
	 */
	public void setShardName(String shardName) {
		this.shardName = shardName;
	}
	public GetShardsThread() {
		super();
	}

	public void run() {
        HorizonsService service = HorizonsService.Factory.getInstance();
		service.addObserver(this);
		try {
			GetShardsMessage msg = null;
			
			if (isGetHZCharacter()) {
				HZCharacter character = service.getNewCharFor(shardName);
				msg = new GetShardsMessage("", false, true);
				msg.setCharacter(character);
			} else {
				String[] shards;
				shards = service.getShards();
				msg = new GetShardsMessage("", false, true);
				msg.setShards(shards);
			}
			setChanged();
			notifyObservers(msg);
		} catch (Exception e) {
			e.printStackTrace();
			setChanged();
			notifyObservers(new GetShardsMessage(e.getMessage(), true));
		}
	}
	
	public void update(Observable arg0, Object arg1) {
		setChanged();
		notifyObservers(new GetShardsMessage((String)arg1));
	}

}

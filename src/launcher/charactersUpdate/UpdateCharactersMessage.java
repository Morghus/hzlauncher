package launcher.charactersUpdate;

import launcher.ObserverMessage;

/*
 * Created on 25.05.2004
 * $Id: UpdateCharactersMessage.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
/**
 * @author unknown
 */
public class UpdateCharactersMessage extends ObserverMessage {


	public UpdateCharactersMessage(String message, boolean error, boolean done) {
		super(message, error, done);
	}
	public UpdateCharactersMessage(String message, boolean error) {
		super(message, error);
	}
	public UpdateCharactersMessage(String message) {
		super(message);
	}

}

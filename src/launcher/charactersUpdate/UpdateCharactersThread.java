package launcher.charactersUpdate;
import java.util.Observable;
import java.util.Observer;

import launcher.Configuration;
import launcher.HZCharacter;
import launcher.HorizonsService;

/*
 * Created on 24.05.2004
 * $Id: UpdateCharactersThread.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
/**
 * @author unknown
 */
public class UpdateCharactersThread extends Observable implements Runnable, Observer {
	/**
	 * 
	 */
	
	public UpdateCharactersThread() {
	}

	public void run() {
		String hostname;
		
		HorizonsService service = HorizonsService.Factory.getInstance();
		service.addObserver(this);
		HZCharacter[] chars = null;
		String publicKey = null;
		try {
			service.logout();
			chars = service.getCharacters();
			publicKey = service.getPublicKey();
			Configuration.getConfig().setHzCharacters(chars);
			Configuration.getConfig().getHzAccount().setPublicKey(publicKey);
			setChanged();
			notifyObservers(new UpdateCharactersMessage("", false, true));
		} catch (Exception e) {
			e.printStackTrace();
			setChanged();
			notifyObservers(new UpdateCharactersMessage(e.getMessage(), true));
		}
	}

	public void update(Observable arg0, Object arg1) {
		setChanged();
		notifyObservers(new UpdateCharactersMessage((String)arg1));
	}
}

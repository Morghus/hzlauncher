package launcher.charactersUpdate;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import launcher.HorizonsLauncher;
import launcher.ProgressDialog;

/*
 * Created on 25.06.2004
 * $Id: CreateCharacterDialog.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */

/**
 * @author unknown
 */
public class CreateCharacterDialog extends JDialog implements Observer {

	/**
	 * 
	 */
	
	ProgressDialog updateDialog;
	HorizonsLauncher parent;
	String[] shards;
	private JComboBox shardBox;
	private GetShardsThread thread;
	
	public CreateCharacterDialog(HorizonsLauncher parent) {		
		super(parent, "Create Character", true);
	
		this.parent = parent;
	
		setSize(175,114);
		setLocationRelativeTo(parent);
		
		updateDialog = new ProgressDialog(parent, "Initializing...");
		
		getShardsList();
	}
	
	private void getShardsList() {

		if (thread == null)
			thread = new GetShardsThread();
		thread.addObserver(this);	
		thread.setGetHZCharacter(false);
		new Thread(thread).start();
		
		updateDialog.setVisible(true);
	}

	public void update(Observable obs, Object arg) {
		if (obs.getClass() == GetShardsThread.class) {
			GetShardsMessage message = (GetShardsMessage) arg;
			
			if (message.isDone() || message.isError())
				updateDialog.setVisible(false);
			
			if (message.isDone()) {				
				if ((shards = message.getShards()) != null) {
					generateShardsContent();
					setVisible(true);
				} else {
					parent.startHorizons(message.getCharacter());
					setVisible(false);
				}
			} else if (message.isError())
				JOptionPane.showMessageDialog(this, "Error: " + message.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			else
				updateDialog.setText(message.getMessage());			
		}		
	}

	private void generateShardsContent() {
		
		ActionListener cancelListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				getDialog().setVisible(false);
			}
		};
		
		ActionListener okListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				createCharacter();
			}
		};

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		JPanel firstRow = new JPanel();
		firstRow.setBorder(BorderFactory.createTitledBorder("Shard"));
		Container secondRow = new Container();
		
		firstRow.setLayout(gridBag);
		secondRow.setLayout(new FlowLayout());
		
		Container cont = getContentPane();
		cont.setLayout(new BorderLayout());
				
		shardBox = new JComboBox(shards);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(shardBox, c);
		firstRow.add(shardBox);
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(okListener);
		secondRow.add(okButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(cancelListener);
		secondRow.add(cancelButton);
		
		cont.add(firstRow, BorderLayout.CENTER);
		cont.add(secondRow, BorderLayout.SOUTH);
	}
	
	/**
	 * 
	 */
	protected void createCharacter() {
		if (thread == null)
			thread = new GetShardsThread();
		thread.setShardName(shards[shardBox.getSelectedIndex()]);		
		thread.setGetHZCharacter(true);
		new Thread(thread).start();
		
		updateDialog.setVisible(true);
	}

	protected JDialog getDialog() {
		return this;
	}
}

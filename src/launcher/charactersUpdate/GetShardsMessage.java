package launcher.charactersUpdate;

import launcher.HZCharacter;
import launcher.ObserverMessage;

/*
 * Created on 25.05.2004
 * $Id: GetShardsMessage.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
/**
 * @author unknown
 */
public class GetShardsMessage extends ObserverMessage {

	public GetShardsMessage(String message, boolean error, boolean done) {
		super(message, error, done);
	}

	public GetShardsMessage(String message, boolean error) {
		super(message, error);
	}

	public GetShardsMessage(String message) {
		super(message);
	}
	
	private String[] shards;
	private HZCharacter character;
	

	/**
	 * @return Returns the character.
	 */
	public HZCharacter getCharacter() {
		return character;
	}
	/**
	 * @param character The character to set.
	 */
	public void setCharacter(HZCharacter character) {
		this.character = character;
	}
	public String[] getShards() {
		return shards;
	}

	public void setShards(String[] shards) {
		this.shards = shards;
	}

}

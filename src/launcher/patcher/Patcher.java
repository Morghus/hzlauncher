package launcher.patcher;
import launcher.Configuration;
import launcher.HorizonsLauncherStrategy;
import launcher.util.URLUtil;
import launcher.util.PersistentHttpConnection;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


/*
 * Created on 25.05.2004
 * $Id: Patcher.java,v 1.18 2006/06/16 23:38:17 oceth Exp $
 */
/**
 * @author unknown
 */
public class Patcher extends Observable implements Observer {
	/**
	 * 
	 */
	private Configuration config;
	private String hzPath;
	private String patchURL;
	private String userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6) Gecko/20040206 Firefox/0.8";
	private PatcherMessage byteMsg = new PatcherMessage("");
	private boolean downloadMasterFileList = true;
	private int totalBytes = 0;
	private String downloadSizeText = "";
	private PatcherGetDownloadSize dlSize = null;
	
	public Patcher(Configuration config) {
		this.config = config;
		hzPath = config.getHorizonsPath();
	}
	
	public boolean checkPatchNecessity(String shard) throws Exception {
		getURLFor(shard);

		byte[] buf = new byte[2048];
		int bytesRead;

		setChanged();
		notifyObservers(new String("Calculating local CRC"));
		
		String crcValue = calculateCRC32("version_info/master_file_list");

		FileInputStream inFile = null;

		downloadFile("version_info/master_file_list_checksum");	
		
		try {
			inFile = new FileInputStream(replaceDirs(hzPath + File.separator + "version_info/master_file_list_checksum"));
		} catch (FileNotFoundException e) {
			return true;
		}		
				
		String inputString = "";
		try {
			while ((bytesRead = inFile.read(buf)) >= 0)
				inputString += new String(buf, 0, bytesRead);
		} catch (IOException e) {
			throw new Exception("Could not read master_file_list_checksum: " + e.getMessage());
		}	
		
		inFile.close();			

		setChanged();
		notifyObservers(new String("Comparing checksums"));
		
		if (!inputString.equals(crcValue))
			return true;
		
		downloadMasterFileList = false;
			
		setChanged();
		notifyObservers(new String("Retrieving version"));
		
		HttpURLConnection con = (HttpURLConnection)new URL(patchURL + "version_info/version.txt").openConnection();
		con.setRequestMethod("GET");		
		con.setRequestProperty("User-Agent", userAgent);
		
		con.connect();
	
		InputStream in = con.getInputStream();
		
		inputString = "";
		while ((bytesRead = in.read(buf)) != -1) {
			inputString += new String(buf, 0, bytesRead);
		}
		
		//con.disconnect(); // kills keep-alive	

		File versionFile = new File(replaceDirs(hzPath + File.separator + "version_info/version.txt"));
		if(!versionFile.exists())
			return true;
		FileInputStream versionIn = new FileInputStream(versionFile);
		
		String remoteString = "";
		try {
			while ((bytesRead = versionIn.read(buf)) >= 0)
				remoteString += new String(buf, 0, bytesRead);
		} catch (IOException e) {
			throw new Exception("Could not read version.txt: " + e.getMessage());
		}
		
		versionIn.close();
		
		if (!remoteString.equals(inputString))
			return true;		
			
		return false;
	}

	/**
	 * @param shard
	 * 
	 */
	public void runFullScan(String shard) throws Exception {		
		getURLFor(shard);
		
		if (downloadMasterFileList) {
			downloadFile("version_info/master_file_list.auc");	
			unpackFile("version_info/master_file_list.auc");
		}
		downloadFile("version_info/no_update_list");
		downloadFile("version_info/no_delete_list");
		
		String line = null;
		int numFiles = 0;
		BufferedReader masterFile = new BufferedReader(new FileReader(replaceDirs(hzPath + File.separator + "version_info/master_file_list")));
		
		line = masterFile.readLine();		
		if (line == null)
			throw new Exception("Could not process master_file_list");
		
		Matcher m = Pattern.compile("^NumFiles=([0-9]*)$").matcher(line);
		if (!m.find())
			throw new Exception("Could not get filecount in master_file_list");
		numFiles = Integer.parseInt(m.group(1));
		
		byteMsg.setOverallBytes(numFiles);
		byteMsg.setOverallBytesPos(0);
		byteMsg.setFiles(true);
		
		LinkedList filesToDownload = new LinkedList();
		
		boolean enableShaderPatcher = HorizonsLauncherStrategy.Factory.isLinux() && Configuration.getConfig().isPatchShaderFiles();
		LinkedList shadersToPatch = new LinkedList();
		
		Pattern p = Pattern.compile("^(.*) < ([0-9]*) < ([0-9]*) < ([0-9a-fA-F]*)$");

		for (int i = 1; (line = masterFile.readLine()) != null; i++) {
			m = p.matcher(line);
			if (!m.find())
				continue;
			String file = m.group(1);
			long fileSize = Long.parseLong(m.group(2));
			/* convert to unix timestamp */
			long fileTime = (Long.parseLong(m.group(3)) - 116444736000000000L) / 10000;
			String fileCRC = m.group(4);

			byteMsg.setMessage("Scanning: " + file);
			byteMsg.setOverallBytesPos(i);
			setChanged();
			notifyObservers(byteMsg);	
			
			if (!compareFile(file, fileSize, fileTime, fileCRC)) {
				filesToDownload.add(new DownloadFile(file, fileTime, fileSize));
				getDownloadSize(replaceDirs(file) + ".auc",fileSize);
			}
			
			if(enableShaderPatcher && file.endsWith(".sha"))
			{
				shadersToPatch.add(file);
			}
		}	
		
		byteMsg.setFiles(false);
		
		byteMsg.setOverallBytes(totalBytes);
		byteMsg.setOverallBytesText(downloadSizeText);
		byteMsg.setOverallBytesPos(0);
		byteMsg.setMessage("");
		setChanged();
		notifyObservers(byteMsg);	
		
		Iterator it = filesToDownload.iterator();
		DownloadFile dlFile;
		
		for(int cur = 0; it.hasNext(); ++cur) {
			byteMsg.setOverallBytes(totalBytes);
			byteMsg.setOverallBytesText(downloadSizeText);
			byteMsg.setCurrentBytesText("Files downloaded: "+cur+"/"+filesToDownload.size());
			dlFile = (DownloadFile)it.next();
			downloadFile(dlFile.getFileName() + ".auc", true);
			unpackFile(dlFile.getFileName() + ".auc");
			setFileDate(dlFile.getFileName(), dlFile.getFileTime());
		}
		
		if(enableShaderPatcher)
		{
			int curFile = 0;
			byteMsg.setByteCounter(false);
			byteMsg.setOverallBytes(shadersToPatch.size());
			byteMsg.setOverallBytesText("");
			byteMsg.setCurrentBytesText("");
			
			for(Iterator pi = shadersToPatch.iterator(); pi.hasNext(); ++curFile)
			{
				String file = (String) pi.next();
				byteMsg.setMessage("Applying addsmooth patch to "+file);
				byteMsg.setOverallBytesPos(curFile);
				setChanged();
				notifyObservers(byteMsg);
				patchShader(file);
			}
		}

		downloadFile("version_info/version.txt");
		downloadFile("version_info/master_file_list_checksum");	
	}

	/**
	 * Applies the addsmooth patch to the specified file
	 * @param file The file to patch
	 */
	private void patchShader(String file) {
		File shf = new File(hzPath + File.separator + replaceDirs(file));
		long time = shf.lastModified();
		StringBuffer content = new StringBuffer();

		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(shf)));
			String line;
			while((line = in.readLine()) != null) {
				content.append(line);
			}
			in.close();
			
			if(content.indexOf("addsmooth") != -1)
			{
				line = content.toString();
				line = line.replaceAll("addsmooth","add");
				
				OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(shf, false));
				out.write(line);
				out.flush();
				out.close();
				
				shf.setLastModified(time);
			}
		} catch(FileNotFoundException e) {
		} catch(IOException e) {
		}
	}

	/**
	 * @param shard
	 * @throws Exception
	 */
	private void getURLFor(String shard) throws Exception {
		if (patchURL != null && patchURL.length() != 0)
			return;

		shard = shard.trim();

		URL url;
		HttpsURLConnection con;
		int bytesRead;
		byte[] buf = new byte[2048];
		String tmp = "";

		PatcherMessage msg = new PatcherMessage("Determining which repository to use...");
		setChanged();
		notifyObservers(msg);

		try {
			url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/lib/run.js");
			con = (HttpsURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("User-Agent", userAgent);
			con.connect();

			BufferedInputStream in = new BufferedInputStream(con.getInputStream());

			while ((bytesRead = in.read(buf)) != -1)
				tmp += new String(buf, 0, bytesRead);

			//con.disconnect(); // kills keep-alive
		} catch (Exception e) {
			throw e;
		}

		Matcher m = Pattern.compile(
				"if \\( shard == \\\"" + shard + "\\\" \\).*?\\{.*?" +
				"Launcher.SetSyncURL \\( \"(.*?)\" \\);.*?" +
				"Launcher.SetHorizonsDirectory \\( \"(.*?)\" \\);.*?\\}", Pattern.DOTALL | Pattern.CASE_INSENSITIVE).matcher(tmp);
		if (!m.find())
			throw new Exception("could not find data for shard \"" + shard + "\"");

		this.patchURL = m.group(1).replaceAll(":21","") + "/" + m.group(2) + "/";
	}

	private void setFileDate(String file, long time) {
		new File(hzPath + File.separator + file).setLastModified(time);		
	}

	private boolean compareFile(String file, long fileSize, long fileTime, String fileCRC) throws Exception {
		File checkFile = new File(hzPath + File.separator + replaceDirs(file));
		if (!checkFile.exists())
			return false;
		if (!checkFileTime(checkFile, fileTime)) {
			String crcValue = calculateCRC32(file);
			if (!fileCRC.equals(crcValue))
				return false;
			else {
				checkFile.setLastModified(fileTime);
				return true;
			}
		}

		if (config.isPerformCRCcheck()) {
			String crcValue = calculateCRC32(file);
			if (!fileCRC.equals(crcValue)) 
				return false;
		}
		
		return true;
	}

	private boolean checkFileTime(File checkFile, long fileTime) {
		if (Math.abs(checkFile.lastModified() - fileTime) <= 1000)
			return true;
		if (Configuration.getConfig().isKeepNewerFiles() && checkFile.lastModified() > fileTime)
			return true;
		return false;
	}

	private void unpackFile(String file) throws Exception {		
		
		file = replaceDirs(file);	

		Matcher m = Pattern.compile("^(.*)\\.auc$").matcher(file);
		if (!m.find())
			throw new Exception("Could not unpack file: invalid file name");
		String dstFile = m.group(1);
		
		ZipFile zip = null;
		File inFile = new File(hzPath + File.separator + file);
		zip = new ZipFile(inFile);

		ZipEntry entry = zip.getEntry(new File(dstFile).getName());
		long lastModified = entry.getTime();
		InputStream zipIn = zip.getInputStream(entry);
		File outFile = new File(hzPath + File.separator + dstFile);
		FileOutputStream fileOut = new FileOutputStream(outFile);
		
		byte[] buf = new byte[2048];
		int bytesRead;
		
		while ((bytesRead = zipIn.read(buf)) != -1)
			fileOut.write(buf, 0, bytesRead);
		
		fileOut.close();
		zipIn.close();
		zip.close();

		if(Configuration.getConfig().isDisableGetFileSizes() && byteMsg != null)
		{
			// adjust overall byte display
			long sizeDiff = outFile.length() - inFile.length();
			byteMsg.setOverallBytesPos(byteMsg.getOverallBytesPos() + (int)sizeDiff);
		}
			
		File dst = new File(hzPath + File.separator + dstFile);
		dst.setLastModified(lastModified);
		
		File src = new File(hzPath + File.separator + file);
		if (!src.delete())
			throw new Exception("Unable to delete file \"" + hzPath + File.separator + file +"\"");
	}
	
	private void downloadFile(String url) throws Exception {
		downloadFile(url, replaceDirs(url), false);
	}
	
	private void downloadFile(String url, String destination) throws Exception {
		downloadFile(url, destination, false);
	}
	
	private void downloadFile(String url, boolean resume) throws Exception {
		downloadFile(url, replaceDirs(url), resume);
	}

	private void downloadFile(String url, String destination, boolean resume) throws Exception {
		FileOutputStream fileOutStream;
		InputStream in = null;
		byte[] buf = new byte[2048];
		int bytesRead;
		long startPos = 0;

		boolean tmp = byteMsg.isByteCounter();
		byteMsg.setByteCounter(true);

		byteMsg.setMessage("Downloading: " + url);
		setChanged();
		notifyObservers(byteMsg);
		
		File fileOut = new File(hzPath + File.separator + destination);
		if (!fileOut.exists()) {
			Matcher m = Pattern.compile("^(.*)\\\\.*$").matcher(hzPath + File.separator + destination);
			if (m.find()) {				
				File filePath = new File(m.group(1));
				if (!filePath.isDirectory())
					filePath.mkdirs();
			}
		} else if (fileOut.exists() && resume) {
			startPos = fileOut.length();
		}
        
        url = url.replace('\\', '/');
		url = URLUtil.encodeUrl(url);

		URL pUrl = new URL(patchURL + url);
		PersistentHttpConnection con = PersistentHttpConnection.Factory.openConnection(pUrl);
		con.setMethod("GET");
		con.setRequestParam("User-Agent", userAgent);
		con.setRequestParam("Connection", "Keep-Alive");
		if (startPos != 0) {
			con.setRequestParam("Range", "bytes=" + startPos + "-");
		}
		
		con.execute(pUrl.getPath());
		
		if (con.getResponseCode() == 416) {
			// 416 - Requested range not satisfiable
			con.finish();
			con = PersistentHttpConnection.Factory.openConnection(pUrl);
			con.setMethod("GET");
			con.setRequestParam("User-Agent", userAgent);
			con.setRequestParam("Connection", "Keep-Alive");
			startPos = 0;
			con.execute(pUrl.getPath());
		}
		
		if ((int)(con.getResponseCode() / 100) == 4 || (int)(con.getResponseCode() / 100) == 5)
			throw new Exception("Server returned HTTP response code: " + con.getResponseCode() + " for URL: " + patchURL + url);
		
		byteMsg.setOverallBytesPos((int) (byteMsg.getOverallBytesPos() + startPos));
		byteMsg.setCurrentBytesPos((int)startPos);
		byteMsg.setCurrentBytes((int)con.getContentLength() + (int)startPos);
		setChanged();
		notifyObservers(byteMsg);
		
		in = con.getInputStream();
			
		try {
			fileOutStream = new FileOutputStream(fileOut, resume);
		} catch (FileNotFoundException e) {
			String path = fileOut.getCanonicalPath();
			String dir = path.substring(0,path.lastIndexOf(File.separatorChar));
			if (!new File(dir).mkdirs())
				throw new Exception("Failed to create directories");
			fileOutStream = new FileOutputStream(fileOut, resume);			
		}
				
		long fetched = startPos;
		long bodySize = con.getContentLength(); 
		while (fetched < bodySize && (bytesRead = in.read(buf)) != -1) {
			byteMsg.setOverallBytes(totalBytes);
			byteMsg.setOverallBytesText(downloadSizeText);
			byteMsg.setCurrentBytesPos(byteMsg.getCurrentBytesPos() + bytesRead);
			byteMsg.setOverallBytesPos(byteMsg.getOverallBytesPos() + bytesRead);
			setChanged();
			notifyObservers(byteMsg);
			fileOutStream.write(buf, 0, bytesRead);
			fetched += bytesRead;
		}

		fileOutStream.close();		

		PersistentHttpConnection.Factory.freeConnection(con);
		
		byteMsg.setByteCounter(tmp);
		
		byteMsg.setCurrentBytes(0);
		byteMsg.setCurrentBytesPos(0);
		setChanged();
		notifyObservers(byteMsg);
	}
	
	private String replaceDirs(String src) {
        if (File.separatorChar == '\\')
            return src.replace('/', File.separatorChar);
        return src.replace('\\', File.separatorChar);
	}



	/**
	 * @param string
	 * @return
	 */
	private String calculateCRC32(String string) throws Exception {
		FileInputStream inFile = null;
		int bytesRead;
		byte[] buf = new byte[2048];

		CRC32 crcFile = new CRC32();
		try {
			inFile = new FileInputStream(hzPath + File.separator + replaceDirs(string));
		} catch (FileNotFoundException e) {
			return "";
		}

		try {
			while ((bytesRead = inFile.read(buf)) >= 0)
				crcFile.update(buf, 0, bytesRead);
		} catch (IOException e) {
			throw new Exception("Could not read master_file_list: " + e.getMessage());
		}

		inFile.close();

		return Long.toHexString(crcFile.getValue());
	}
	
	private void getDownloadSize(String string, long size) throws Exception {
		if (Configuration.getConfig().isDisableGetFileSizes())
		{
			update(null,new Integer((int)size));
			return;
		}
		if (dlSize == null) {
			dlSize = new PatcherGetDownloadSize(patchURL, userAgent);
			dlSize.addObserver(this);
		}
		dlSize.runGetSize(string);		
	}

	public void update(Observable o, Object arg) {
		if (arg.getClass() == Integer.class)
			totalBytes += ((Integer)arg).intValue();
		else if(arg.getClass() == String.class)
			downloadSizeText = (String) arg;
	}	
}

package launcher.patcher;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;
/*
 * Created on 13.06.2004
 * $Id: PatcherGetDownloadSize.java,v 1.4 2006/06/16 23:38:17 oceth Exp $
 */

/**
 * @author unknown
 */
public class PatcherGetDownloadSize extends Observable implements Observer {

	/**
	 * 
	 */
	public static final int MAX_THREADS = 6;
	private String patchURL;
	private String userAgent;
	private LinkedList queue;
	private Vector threads;
	private int totalFiles = 0;
	
	
	/**
	 * @param file
	 * @param patchURL
	 * @param userAgent
	 */
	public PatcherGetDownloadSize(String patchURL, String userAgent) {
		super();
		this.patchURL = patchURL;
		this.userAgent = userAgent;
		queue = new LinkedList();
		threads = new Vector();
		System.setProperty("http.keepAlive", "true");
		System.setProperty("http.maxConnections", ""+(MAX_THREADS*2));
	}
	
	public void runGetSize(String file) {
		synchronized(queue)
		{
			if (threads.size() < MAX_THREADS) {
				startThread(file);
			} else {
				queue.add(file);
			}
			totalFiles++;
		}
	}
	
	public String getNextFileOrFinish(PatcherGetDownloadSizeThread thread)
	{
		synchronized(queue)
		{
			if(queue.size() > 0)
			{
				return (String) queue.removeFirst();
			}
			else
			{
				if(thread != null)
				{
					threads.remove(thread);
				}
				return null;
			}
		}
	}

	private synchronized void startThread(String file) {
		PatcherGetDownloadSizeThread thread = new PatcherGetDownloadSizeThread(this, file, patchURL, userAgent);
		thread.addObserver(this);
		Thread rt = new Thread(thread);
		threads.add(thread);
		rt.start();
	}

	public synchronized void update(Observable o, Object arg) {
		if (arg.getClass() == Integer.class) {
			setChanged();
			notifyObservers(arg);
			setChanged();
			notifyObservers("Filesizes retrieved: "+(totalFiles-queue.size()-threads.size())+"/"+totalFiles);
		}
	}
}

package launcher.patcher;
import launcher.util.PersistentHttpConnection;
import launcher.util.URLUtil;

import java.net.URL;
import java.net.MalformedURLException;
import java.util.Observable;

/*
 * Created on 14.06.2004
 * $Id: PatcherGetDownloadSizeThread.java,v 1.5 2006/06/16 23:38:17 oceth Exp $
 */

/**
 * @author unknown
 */
public class PatcherGetDownloadSizeThread extends Observable implements Runnable {

	private String file;
	private String patchURL;
	private String domain;
	private int port;
	private String userAgent;
	private PatcherGetDownloadSize master;
	private PersistentHttpConnection conn;

	public PatcherGetDownloadSizeThread(PatcherGetDownloadSize master, String file, String patchURL,
	                                    String userAgent) {
		super();
		this.file = file;
		this.patchURL = patchURL;
		this.userAgent = userAgent;
		this.master = master;
		try {
			URL tmp = new URL(patchURL);
			port = tmp.getPort();
			if(port == -1) port = tmp.getDefaultPort();
			domain = tmp.getHost();
		} catch(MalformedURLException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
	
	public void run() {
		conn = PersistentHttpConnection.Factory.openConnection(domain, port);
		do
		{
			getSize();
			this.file = master.getNextFileOrFinish(this);
		} while(this.file != null);
		PersistentHttpConnection.Factory.freeConnection(conn);
	}
	
	private void getSize()
	{
		int ret = 0;	
	
		try {
			conn.setMethod("HEAD");
			conn.setRequestParam("Connection", "Keep-Alive");
			conn.setRequestParam("User-Agent", userAgent);
			conn.execute(URLUtil.encodeUrl(new URL(patchURL + file).getPath()));
			String size = conn.getResponseParam("Content-Length");
			if(size != null)
				ret = Integer.parseInt(size);
			conn.finish();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		setChanged();
		notifyObservers(new Integer(ret));		
	}
}

package launcher.patcher;
import java.util.Observable;
import java.util.Observer;

import launcher.Configuration;


/*
 * Created on 25.05.2004
 * $Id: PatcherThread.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
/**
 * @author unknown
 */
public class PatcherThread extends Observable implements Runnable, Observer {

	private boolean checkPatchNecessity;
	private boolean alwaysPatch;
	private String shard;
	
	public PatcherThread() {
	}

	public void run() {
		Patcher patcher = new Patcher(Configuration.getConfig());
		patcher.addObserver(this);
		
		if (shard == null) {
			setChanged();
			notifyObservers(new PatcherMessage("no shard selected", true));
			return;
		}
		
		try {
			if (patcher.checkPatchNecessity(shard) || alwaysPatch) {
				patcher.runFullScan(shard);
				setChanged();
				PatcherMessage msg = new PatcherMessage("");
				msg.setPatchingNecessary(true);
				msg.setDone(true);
				notifyObservers(msg);
			} else {
				setChanged();
				PatcherMessage msg = new PatcherMessage("");
				msg.setPatchingNecessary(false);
				msg.setDone(true);
				notifyObservers(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			setChanged();
			notifyObservers(new PatcherMessage(e.getMessage(), true));
		}
	}
	
	public void update(Observable obs, Object arg) {
		setChanged();
		if (arg.getClass() == PatcherMessage.class) {
			PatcherMessage msg = (PatcherMessage)arg;
			notifyObservers(msg);
		} else if (arg.getClass() == String.class) {
			notifyObservers(new PatcherMessage((String)arg));
		}
	}
	
	/**
	 * @return Returns the checkPatchNecessity.
	 */
	public boolean isCheckPatchNecessity() {
		return checkPatchNecessity;
	}
	/**
	 * @param checkPatchNecessity The checkPatchNecessity to set.
	 */
	public void setCheckPatchNecessity(boolean checkPatchNecessity) {
		this.checkPatchNecessity = checkPatchNecessity;
	}
	/**
	 * @return Returns the alwaysPatch.
	 */
	public boolean isAlwaysPatch() {
		return alwaysPatch;
	}
	/**
	 * @param alwaysPatch The alwaysPatch to set.
	 */
	public void setAlwaysPatch(boolean alwaysPatch) {
		this.alwaysPatch = alwaysPatch;
	}

	/**
	 * @param shard
	 */
	public void setShard(String shard) {
		this.shard = shard;
	}
}

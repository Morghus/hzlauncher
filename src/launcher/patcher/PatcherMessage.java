package launcher.patcher;
/*
 * Created on 25.05.2004
 * $Id: PatcherMessage.java,v 1.3 2006/06/16 18:28:30 oceth Exp $
 */
/**
 * @author unknown
 */
public class PatcherMessage {
	/**
	 * 
	 */
	private String message;
	private boolean error;
	private boolean done;
	private boolean patchingNecessary;
	private boolean byteCounter;
	private int overallBytes = 0;
	private int currentBytes = 0;
	private int currentBytesPos = 0;
	private String currentBytesText = "";
	private int overallBytesPos = 0;
	private String overallBytesText = "";
	private float bytesPerSec = 0;
	private int timeLeft = 0;
	private boolean files = false;
	
	/**
	 * @return Returns the files.
	 */
	public boolean isFiles() {
		return files;
	}
	/**
	 * @param files The files to set.
	 */
	public void setFiles(boolean files) {
		this.files = files;
	}
	/**
	 * @return Returns the currentBytes.
	 */
	public int getCurrentBytes() {
		return currentBytes;
	}
	/**
	 * @param currentBytes The currentBytes to set.
	 */
	public void setCurrentBytes(int currentBytes) {
		this.currentBytes = currentBytes;
	}
	/**
	 * @return Returns the currentBytesPos.
	 */
	public int getCurrentBytesPos() {
		return currentBytesPos;
	}
	/**
	 * @param currentBytesPos The currentBytesPos to set.
	 */
	public void setCurrentBytesPos(int currentBytesPos) {
		this.currentBytesPos = currentBytesPos;
	}

	public String getCurrentBytesText() {
		return currentBytesText;
	}

	public void setCurrentBytesText(String currentBytesText) {
		this.currentBytesText = currentBytesText;
	}

	/**
	 * @return Returns the overallBytes.
	 */
	public int getOverallBytes() {
		return overallBytes;
	}
	/**
	 * @param overallBytes The overallBytes to set.
	 */
	public void setOverallBytes(int overallBytes) {
		this.overallBytes = overallBytes;
	}
	/**
	 * @return Returns the overallBytesPos.
	 */
	public int getOverallBytesPos() {
		return overallBytesPos;
	}
	/**
	 * @param overallBytesPos The overallBytesPos to set.
	 */
	public void setOverallBytesPos(int overallBytesPos) {
		this.overallBytesPos = overallBytesPos;
	}

	/**
	 * @return Returns the overallBytesText
	 */
	public String getOverallBytesText() {
		return overallBytesText;
	}

	/**
	 * @param overallBytesText The overallBytesText to set
	 */
	public void setOverallBytesText(String overallBytesText) {
		this.overallBytesText = overallBytesText;
	}

	/**
	 * @return Returns the patchingNecessary.
	 */
	public boolean isPatchingNecessary() {
		return patchingNecessary;
	}
	/**
	 * @param patchingNecessary The patchingNecessary to set.
	 */
	public void setPatchingNecessary(boolean patchingNecessary) {
		this.patchingNecessary = patchingNecessary;
	}
	/**
	 * @return Returns the done.
	 */
	public boolean isDone() {
		return done;
	}
	/**
	 * @param done The done to set.
	 */
	public void setDone(boolean done) {
		this.done = done;
	}
	/**
	 * @return Returns the error.
	 */
	public boolean isError() {
		return error;
	}
	/**
	 * @param error The error to set.
	 */
	public void setError(boolean error) {
		this.error = error;
	}
	/**
	 * @param message
	 * @param error
	 */
	public PatcherMessage(String message, boolean error) {
		super();
		this.message = message;
		this.error = error;
	}
	/**
	 * @param message
	 * @param error
	 * @param done
	 */
	public PatcherMessage(String message, boolean error, boolean done) {
		super();
		this.message = message;
		this.error = error;
		this.done = done;
	}
	public PatcherMessage(String message) {
		this.message = message;
	}
	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message The message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return Returns the byteCounter.
	 */
	public boolean isByteCounter() {
		return byteCounter;
	}
	/**
	 * @param byteCounter The byteCounter to set.
	 */
	public void setByteCounter(boolean byteCounter) {
		this.byteCounter = byteCounter;
	}
	/**
	 * @return Returns the bytesPerSec.
	 */
	public float getBytesPerSec() {
		return bytesPerSec;
	}
	/**
	 * @param bytesPerSec The bytesPerSec to set.
	 */
	public void setBytesPerSec(float bytesPerSec) {
		this.bytesPerSec = bytesPerSec;
	}
	/**
	 * @return Returns the timeLeft.
	 */
	public int getTimeLeft() {
		return timeLeft;
	}
	/**
	 * @param timeLeft The timeLeft to set.
	 */
	public void setTimeLeft(int timeLeft) {
		this.timeLeft = timeLeft;
	}
}

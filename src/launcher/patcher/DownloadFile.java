package launcher.patcher;
/*
 * Created on 25.05.2004
 * $Id: DownloadFile.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
/**
 * @author unknown
 */
public class DownloadFile {
	private String fileName;
	private long fileTime;
	private long fileSize;
	/**
	 * @param fileName
	 * @param fileTime
	 */
	public DownloadFile(String fileName, long fileTime, long fileSize) {
		this.fileName = fileName;
		this.fileTime = fileTime;
		this.fileSize = fileSize;
	}
	/**
	 * @return Returns the fileName.
	 */
	public String getFileName() {
		return fileName;
	}
	/**
	 * @param fileName The fileName to set.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	/**
	 * @return Returns the fileTime.
	 */
	public long getFileTime() {
		return fileTime;
	}
	/**
	 * @param fileTime The fileTime to set.
	 */
	public void setFileTime(int fileTime) {
		this.fileTime = fileTime;
	}


	public long getFileSize()
	{
		return fileSize;
	}

	public void setFileSize(long fileSize)
	{
		this.fileSize = fileSize;
	}
}

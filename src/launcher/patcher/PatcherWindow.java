package launcher.patcher;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;


/*
 * Created on 25.05.2004
 * $Id: PatcherWindow.java,v 1.3 2006/06/16 18:28:30 oceth Exp $
 */
/**
 * @author unknown
 */
public class PatcherWindow extends JDialog implements Observer {
	/**
	 * 
	 */
	private JProgressBar thisProgress;
	private JProgressBar overallProgress;
	private JLabel mainText;
	
	private boolean isPatchingNecessary;
	private boolean error = false;
	private Thread patchInstance;
	private JLabel statusText;

	public PatcherWindow(Frame parent) {
		super(parent);
		
		WindowListener windowListener = new WindowListener() {
			public void windowClosing(WindowEvent arg0) {
				error = true;
				patchInstance.stop();
				setVisible(false);
			}
			public void windowActivated(WindowEvent arg0) {}
			public void windowClosed(WindowEvent arg0) {}
			public void windowDeactivated(WindowEvent arg0) {}
			public void windowDeiconified(WindowEvent arg0) {}
			public void windowIconified(WindowEvent arg0) {}
			public void windowOpened(WindowEvent arg0) {}		
		};
		
		ActionListener closingListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				error = true;
				patchInstance.stop();
				setVisible(false);
			}
		};
		
		setTitle("Patching...");
		setSize(350, 100);
		setModal(true);
		addWindowListener(windowListener);
		setLocationRelativeTo(parent);
		
		Container cont = getContentPane();
		cont.setLayout(new BorderLayout());
		
		Container statusContainer = new Container();
		statusContainer.setLayout(new BorderLayout());
		
		Container progressContainer = new Container();
		progressContainer.setLayout(new GridLayout(2,1));
		
		thisProgress = new JProgressBar();
		progressContainer.add(thisProgress);
		
		overallProgress = new JProgressBar();
		progressContainer.add(overallProgress);
		
		statusContainer.add(progressContainer, BorderLayout.CENTER);
		Container cancelContainer = new Container();
		cancelContainer.setLayout(new FlowLayout());
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(closingListener);
		cancelContainer.add(cancelButton);
		statusContainer.add(cancelContainer, BorderLayout.EAST);		
		
		Container mainContent = new Container();
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		mainContent.setLayout(gridBag);
		
		mainText = new JLabel();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(mainText, c);
		mainContent.add(mainText);
		
		statusText = new JLabel();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(statusText, c);
		mainContent.add(statusText);
		
		cont.add(statusContainer, BorderLayout.SOUTH);
		cont.add(mainContent, BorderLayout.CENTER);

	}
	
	public boolean runPatch(boolean b, String shard) {
		PatcherThread patcherThread = new PatcherThread();
		patcherThread.setAlwaysPatch(b);
		patcherThread.addObserver(this);
		patcherThread.setShard(shard);
		patchInstance = new Thread(patcherThread);
		patchInstance.start();
		
		setVisible(true);
		
		if (isError())
			return false;
		return true;		
	}
	
	public void update(Observable obs, Object arg) {
		PatcherMessage msg = (PatcherMessage)arg;
		
		if (msg.isError()) {
			setError(true);
			JOptionPane.showMessageDialog(this, "Error: " + msg.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
			
		if (msg.isDone() || msg.isError())
			setVisible(false);
		
		if (msg.getMessage().length() > 0)
			mainText.setText(msg.getMessage());
		
		if (msg.getOverallBytes() != 0) {
			DecimalFormat fmt = new DecimalFormat("#,###,##0.00");
			if (msg.isByteCounter()) {
				statusText.setText(fmt.format(msg.getOverallBytesPos() / 1024.0) + " kb / " + fmt.format(msg.getOverallBytes() / 1024.0) + " kb");
			} else
				statusText.setText(msg.getOverallBytesPos() + " / " + msg.getOverallBytes() + " files");				
		} else if (msg.getCurrentBytes() != 0 && msg.getOverallBytes() == 0) {
			DecimalFormat fmt = new DecimalFormat("#,###,##0.00");
			statusText.setText(fmt.format(msg.getCurrentBytesPos() / 1024.0) + " kb / " + fmt.format(msg.getCurrentBytes() / 1024.0) + " kb");			
		} else if (msg.getCurrentBytes() == 0 && msg.getOverallBytes() == 0) {
			statusText.setText("");
		}
		
		thisProgress.setMaximum(msg.getCurrentBytes());
		thisProgress.setValue(msg.getCurrentBytesPos());
		thisProgress.setString(msg.getCurrentBytesText());
		thisProgress.setStringPainted(msg.getCurrentBytesText() != null && !msg.getCurrentBytesText().equals(""));
		
		overallProgress.setMaximum(msg.getOverallBytes());
		overallProgress.setValue(msg.getOverallBytesPos());
		overallProgress.setString(msg.getOverallBytesText());
		overallProgress.setStringPainted(msg.getOverallBytesText() != null && !msg.getOverallBytesText().equals(""));
	}

	/**
	 * @param b
	 */
	private void setError(boolean b) {
		error = b;		
	}
	
	/**
	 * @return Returns the error.
	 */
	public boolean isError() {
		return error;
	}
}

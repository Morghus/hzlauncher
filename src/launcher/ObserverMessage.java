package launcher;
/*
 * Created on 25.06.2004
 * $Id: ObserverMessage.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */

/**
 * @author unknown
 */
public abstract class ObserverMessage {
	private String message;
	private boolean error;
	private boolean done;
	
	public ObserverMessage(String message) {
		this.message = message;
	}
	/**
	 * @return Returns the error.
	 */
	public boolean isError() {
		return error;
	}
	/**
	 * @param error The error to set.
	 */
	public void setError(boolean error) {
		this.error = error;
	}
	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message The message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return Returns the done.
	 */
	public boolean isDone() {
		return done;
	}
	/**
	 * @param done The done to set.
	 */
	public void setDone(boolean done) {
		this.done = done;
	}
	/**
	 * @param message
	 * @param error
	 */
	public ObserverMessage(String message, boolean error) {
		super();
		this.message = message;
		this.error = error;
	}
	/**
	 * @param message
	 * @param error
	 * @param done
	 */
	public ObserverMessage(String message, boolean error, boolean done) {
		super();
		this.message = message;
		this.error = error;
		this.done = done;
	}
}

/*
 * Created on 06.07.2004
 * $Id: ProgressDialog.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
package launcher;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;

import javax.swing.JDialog;
import javax.swing.JLabel;


/**
 * @author unknown
 */
public class ProgressDialog extends JDialog {

	/**
	 * 
	 */
	private JLabel updateLabel;
	private Window parent;
	
	public ProgressDialog(Frame parent) {
		super(parent);
		this.parent = parent;
		initialize("");
	}
	
	public ProgressDialog(Frame parent, String title) {
		super(parent);
		this.parent = parent;
		initialize(title);
	}
	
	public ProgressDialog(Dialog parent) {
		super(parent);
		this.parent = parent;
		initialize("");
	}

	public ProgressDialog(Dialog parent, String title) {
		super(parent);
		this.parent = parent;
		initialize(title);
	}

	/**
	 * 
	 */
	private void initialize(String title) {
		setTitle(title);
		setModal(true);
		setSize(215, 65);
		setLocationRelativeTo(parent);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		
		Container cont = getContentPane();
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		cont.setLayout(gridBag);		
		
		Container center = new Container();
		center.setLayout(new FlowLayout());
		
		updateLabel = new JLabel("", JLabel.CENTER);
		center.add(updateLabel);
		center.add(new ProgressIcon());
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.weightx = 1.0;
		gridBag.setConstraints(center, c);
		cont.add(center);	
	}
	
	public void setText(String text) {
		updateLabel.setText(text);
	}
	
	public String getText() {
		return updateLabel.getText();
	}
}

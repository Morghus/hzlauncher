/*
 * Created on 21.08.2004
 * $Id: SelectShardWindow.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
package launcher;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * @author unknown
 */
public class SelectShardWindow extends JDialog {

	private HorizonsLauncher parent;
	private JComboBox shardBox;
	private String selectedShard;
	
	public SelectShardWindow(HorizonsLauncher parent) {
		super(parent, "Select Shard", true);
		
		this.parent = parent;
	
		setSize(175,114);
		setLocationRelativeTo(parent);
		
		String[] shards = Configuration.getConfig().getCharacterShards();
		if (shards == null) {
			JOptionPane.showMessageDialog(this, "No Characters created, populating some default shards!", "Error", JOptionPane.ERROR_MESSAGE);
			shards = new String[] { "Order","Chaos","Blight"};
		}
		
		ActionListener cancelListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		};
		
		ActionListener okListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setSelectedShard((String)shardBox.getSelectedItem());
				setVisible(false);
			}
		};

		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		JPanel firstRow = new JPanel();
		firstRow.setBorder(BorderFactory.createTitledBorder("Shard"));
		Container secondRow = new Container();
		
		firstRow.setLayout(gridBag);
		secondRow.setLayout(new FlowLayout());
		
		Container cont = getContentPane();
		cont.setLayout(new BorderLayout());
				
		shardBox = new JComboBox(shards);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(shardBox, c);
		firstRow.add(shardBox);
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(okListener);
		secondRow.add(okButton);

		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(cancelListener);
		secondRow.add(cancelButton);
		
		cont.add(firstRow, BorderLayout.CENTER);
		cont.add(secondRow, BorderLayout.SOUTH);
		
		setVisible(true);
	}

	protected void setSelectedShard(String string) {
		this.selectedShard = string;
	}

	public String getSelectedShard() {
		return selectedShard;
	}
}

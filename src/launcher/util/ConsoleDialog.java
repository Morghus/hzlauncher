package launcher.util;

import launcher.Configuration;

import javax.swing.*;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;

/**
 * @author Oceth
 */
public class ConsoleDialog extends JDialog {
	private Console console;

	public ConsoleDialog(JFrame launcherWindow, Process p, String[] cmd) {
		super(launcherWindow, "Horizons output console", true);
		this.addWindowListener(
				new WindowListener() {
					public void windowClosing(WindowEvent arg0) {
						Configuration.saveConfig();
						dispose();
					}

					public void windowActivated(WindowEvent arg0) {
					}

					public void windowClosed(WindowEvent arg0) {
					}

					public void windowDeactivated(WindowEvent arg0) {
					}

					public void windowDeiconified(WindowEvent arg0) {
					}

					public void windowIconified(WindowEvent arg0) {
					}

					public void windowOpened(WindowEvent arg0) {
					}
				}
		);
		StringBuffer cl = new StringBuffer();
		cl.append("CMDLine:\n");
		for(int i = 0; i<cmd.length; ++i) {
			cl.append("\t"); cl.append(cmd[i]); cl.append("\n");
		}
		console = new Console(p.getInputStream(), p.getErrorStream(), cl.toString());
		JScrollPane scroller = new JScrollPane(console);
		this.getContentPane().add(scroller);
		this.setSize(640, 480);
		this.setResizable(true);
	}
	
	public void show()
	{
		console.startUpdater();
		super.show();
		console.killThreads();
	}
}

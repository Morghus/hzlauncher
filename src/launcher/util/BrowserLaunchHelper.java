package launcher.util;

import edu.stanford.ejalbert.BrowserLauncher;

import javax.swing.*;
import java.net.URL;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.*;

/**
 * @author oceth
 */
public class BrowserLaunchHelper {
	public static void openUrl(URL url) {
		openUrl(url.toString());
	}
	public static void openUrl(String url) {
		try {
			BrowserLauncher bl = new BrowserLauncher();
			bl.openURLinBrowser(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static JLabel makeKlickable(String url, JLabel label) {
		label.addMouseListener(new URLMouseListener(url));
		label.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		return label;
	}

	private static class URLMouseListener implements MouseListener {
		private String url;

		public URLMouseListener(String url) {
			this.url = url;
		}

		public void mouseClicked(MouseEvent e) {
			openUrl(url);
		}

		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
}

package launcher.util;

import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.*;

/**
 * Provides HTTP Keep-Alive functionality
 * @author Oceth
 */
public class PersistentHttpConnection {
	private static final String NL = "\r\n";
	private String domain;
	private int port;
	private Socket conn = null;
	private String method = "GET";
	private Map requestParams;
	private Map responseParams;
	private int responseCode;
	private long contentLength;

	protected PersistentHttpConnection(String domain, int port) {
		this.domain = domain;
		this.port = port;

		requestParams = new HashMap();
		responseParams = new HashMap();
		requestParams.put("Host",domain);
	}

	public void setRequestParam(String name, String value)
	{
		requestParams.put(name,value);
	}

	public String getRequestParam(String name)
	{
		return (String)requestParams.get(name);
	}

	public String getResponseParam(String name)
	{
		return (String)responseParams.get(name);
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public InputStream getInputStream() throws IOException {
		return conn.getInputStream();
	}

	public int getResponseCode() {
		return responseCode;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void execute(String path) throws IOException {
		finish();

		StringBuffer reqData = new StringBuffer();
		StringBuffer respData = new StringBuffer();
		if(conn == null) connect();
		OutputStream out = conn.getOutputStream();

		// prepare request
		reqData.append(method+" "+path+" HTTP/1.0"+NL);
		for(Iterator pi = requestParams.keySet().iterator(); pi.hasNext();)
		{
			String param = (String) pi.next();
			if(requestParams.get(param) != null)
			{
				reqData.append(param+": "+requestParams.get(param)+NL);
			}
		}
		reqData.append(NL);

		try
		{
			out.write(reqData.toString().getBytes("ASCII"));
		}
		catch(IOException e)
		{
			conn.close();
			connect();
			out = conn.getOutputStream();
			out.write(reqData.toString().getBytes("ASCII"));
		}

		out.flush();

		InputStream in = conn.getInputStream();

		int bytes;
		byte[] buf = new byte[1];
		char cur = ' ';
		char[] prev = new char[] {' ',' ',' ',' '};
		boolean foundEndOfHeader = false;
		int headerIndex = 0;

		while((bytes = in.read(buf)) != -1)
		{
			for(int i = 0; i < bytes; ++i)
			{
				if(headerIndex > 3)
					respData.append(prev[0]);
				prev[0] = prev[1];
				prev[1] = prev[2];
				prev[2] = prev[3];
				cur = (char) buf[0];
				prev[3] = cur;
			}
			if(new String(prev).equals(NL+NL))
			{
				foundEndOfHeader = true;
				break;
			}
			++headerIndex;
		}

		if(!foundEndOfHeader)
		{
			invalidateConnection();
			throw new IOException("Error: response incomplete\nRequest:\n"+reqData+"\nResponse:\n"+respData+new String(prev));
		}
		else try
		{
			String head = respData.toString();

			String[] line = head.split(NL);
			String[] tmp = line[0].split(" ");
			responseCode = Integer.parseInt(tmp[1]);

			for(int i = 1; i < line.length; ++i)
			{
				tmp = line[i].split(": ");
				responseParams.put(tmp[0], tmp[1]);
				if(tmp[0].equalsIgnoreCase("Content-Length"))
				{
					contentLength = Long.parseLong(tmp[1]);
				}
			}


		}
		catch(Exception e)
		{
			invalidateConnection();
			throw new IOException("Error parsing response:\n"+respData+"\nRequest was:\n"+reqData);
		}
	}

	private void connect() throws IOException {
		try
		{
			conn = new Socket(domain, port);
		}
		catch(Exception e)
		{
			throw new IOException(e.toString());
		}
	}
	
	private void invalidateConnection()
	{
		try
		{
			conn.close();
		}
		catch(Exception e)
		{
			// discard
		}
		conn = null;
	}

	public void finish()
	{
		if(responseCode >= 400) // Invalidate Keep-Alive
		{
			invalidateConnection();
		}
		responseParams.clear();
		responseCode = -1;
		contentLength = -1;
	}

	public static class Factory
	{
		private static int MAX_FREE_CONNECTIONS = 5;
		private static List freeConnections = new LinkedList();

		public static PersistentHttpConnection openConnection(String domain, int port)
		{
			synchronized(freeConnections)
			{
				for(Iterator ci = freeConnections.iterator(); ci.hasNext();)
				{
					PersistentHttpConnection c = (PersistentHttpConnection) ci.next();
					if(c.domain.equalsIgnoreCase(domain) && c.port == port)
					{
						freeConnections.remove(c);
						return c;
					}
				}
			}
			return new PersistentHttpConnection(domain, port);
		}

		public static PersistentHttpConnection openConnection(URL url)
		{
			String domain;
			int port;
			port = url.getPort();
			if(port == -1) port = url.getDefaultPort();
			domain = url.getHost();
			return openConnection(domain, port);
		}

		public static void freeConnection(PersistentHttpConnection c)
		{
			synchronized(freeConnections)
			{
				if(freeConnections.size() >= MAX_FREE_CONNECTIONS)
				{
					PersistentHttpConnection old = (PersistentHttpConnection) freeConnections.remove(0);
					try
					{
						old.conn.close();
					}
					catch(Exception e)
					{
						// discard
					}
				}
				freeConnections.add(c);
			}
		}
	}
}

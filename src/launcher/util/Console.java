package launcher.util;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Oceth
 */
public class Console extends JEditorPane {
	protected static final String APPEND_MARK = "<append>";
	private TextAdder errReader;
	private TextAdder outReader;
	private TextUpdater updater;
	private String text;
	private boolean textUpdated = false;

	public Console(InputStream out, InputStream err) {
		this(out,err,null);
	}

	public Console(InputStream out, InputStream err, String initialText) {
		this.setEditable(false);
		this.setContentType("text/html");
		this.text = "<html><body>"+((initialText != null && initialText.length()>0)?"<pre>"+initialText+"</pre><br><br><br>":"")+"Output:<pre>"+APPEND_MARK+"</pre></body></html>";
		this.textUpdated = true;
		this.errReader = new TextAdder(err, true);
		this.outReader = new TextAdder(out, false);
		this.updater = new TextUpdater();

		this.errReader.start();
		this.outReader.start();
	}

	public void addText(String text, boolean err)
	{
		synchronized(this.text)
		{
			String start = "<font color=\""+(err?"red":"blue")+"\">";
			String end = "</font>\n";
			int mark = this.text.indexOf(APPEND_MARK);
			if(mark != -1)
			{
				String open = this.text.substring(0, mark);
				String close = this.text.substring(mark);
				this.text = open+start+text+end+close;  
			}
			else
			{
				this.text += "appender error";
			}
			this.textUpdated = true;
		}
	}
	
	public void startUpdater()
	{
		this.updater.start();
	}
	
	public void updateText()
	{
		synchronized(this.text)
		{
			if(this.textUpdated)
			{
				this.setText(this.text);
				try {Thread.sleep(100);} catch(InterruptedException e) {}
				Rectangle view = new Rectangle(0, this.getHeight()-1, 1, 1);
				this.scrollRectToVisible(view);
				this.textUpdated = false;
			}
		}
	}

	public void killThreads() {
		this.updater.stop();
		this.errReader.stop();
		this.outReader.stop();
	}

	class TextAdder extends Thread
	{
		private BufferedReader in;
		private boolean err;

		public TextAdder(InputStream in, boolean err) {
			super("TextAdder");
			this.in = new BufferedReader(new InputStreamReader(in));
			this.err = err;
		}


		public void run()
		{
			try {
				String line = in.readLine();
				while(line != null) {
					addText(line, err);

					try {
						sleep(50);
					} catch(InterruptedException e) {
					}


					line = in.readLine();
				}

			} catch(IOException e) {
				addText(e.toString(), true);
			}
			addText("Std"+(err?"Err":"Out")+" closed", err);
		}
	}
	
	class TextUpdater extends Thread
	{
		public TextUpdater()
		{
			super("text updater");
		}
		
		public void run()
		{
			while(true)
			{
				updateText();
				try {
					sleep(250);
				} catch(InterruptedException e) {
					
				}
			}
		}
	}
}

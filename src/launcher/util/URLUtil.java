package launcher.util;

import java.net.URLEncoder;
import java.io.UnsupportedEncodingException;

/**
 * @author Oceth
 */
public class URLUtil {
	public static String encodeUrl(String url) {
		String domainPart = "";
		String newUrl;
		String[] parts;
		if(url.matches("https?://[a-zA-Z\\.]+(/.*)?"))
		{
			domainPart = url.substring(0,url.indexOf("/", 9));
			newUrl = url.substring(url.indexOf("/", 9));
		}
		else
		{
			newUrl = url;
		}
		parts = newUrl.split("/");
		newUrl = domainPart;
		try {
			for(int i = 0; i < parts.length; ++i) {
				if(i > 0) newUrl += "/";
				newUrl += URLEncoder.encode(parts[i], "UTF-8");
			}
		} catch(UnsupportedEncodingException e) {
			newUrl = url;
		}
		return newUrl.replaceAll("\\+","%20"); // a bit hacky, but works
	}
}

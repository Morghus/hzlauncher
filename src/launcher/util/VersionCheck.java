package launcher.util;

import static launcher.util.BrowserLaunchHelper.makeKlickable;

import javax.swing.*;
import java.net.URL;
import java.net.MalformedURLException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

/**
 * @author oceth
 */
public class VersionCheck implements Runnable{
	private static final String VERSION_BASE_URL = "http://horizons.oceth.net/version/java-launcher.xml";

	private String localVersion;
	private String remVersion;
	private JFrame parent;

	public VersionCheck(JFrame parent, String localVersion) {
		this.localVersion = localVersion;
		this.parent = parent;
	}

	public void check() {
		Thread t = new Thread(this);
		t.start();
	}

	public void run() {
		remVersion = localVersion;
		try {
			URL vUrl = new URL(VERSION_BASE_URL+"/"+localVersion);
			BufferedReader in = new BufferedReader(new InputStreamReader(vUrl.openStream()));
			remVersion = in.readLine();
			in.close();
			remVersion = remVersion.replaceAll("[ \r\n\t]","");
		} catch (Exception e) {
			e.printStackTrace();
		}

		if(!remVersion.equals(localVersion))
		{
			showNewVersionInfo();
		}
	}

	protected void showNewVersionInfo()
	{
		final JDialog about = new JDialog(parent, "New Horizons Launcher version available", true);

		ActionListener okListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				about.setVisible(false);
			}
		};

		Container cont = about.getContentPane();
		cont.setLayout(new GridLayout(0,1));

		JLabel launcherLabel = new JLabel("Horizons Launcher\n");
		launcherLabel.setFont(new Font("Tahoma", Font.BOLD, 14));

		JButton okButton = new JButton("OK");
		okButton.addActionListener(okListener);

		cont.add(launcherLabel);
		cont.add(new JLabel("Version " + remVersion));
		cont.add(new JLabel("available for download from:"));
		cont.add(makeKlickable("http://horizons.oceth.net/",new JLabel("<html><body><a href=\"http://horizons.oceth.net/\">http://horizons.oceth.net/</a></body></html>")));
		cont.add(new JLabel("USE AT YOUR OWN RISK!"));
		cont.add(new JLabel("You can disable this warning in the preferences."));
		cont.add(okButton);

		about.pack();
		//about.setSize(320, 230);
		about.setResizable(false);
		about.setLocationRelativeTo(parent);
		about.setVisible(true);
	}
}

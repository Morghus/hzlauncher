package launcher.util;

import launcher.Configuration;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

/**
 * @author Oceth
 */
public class CmdRunner {
	private String[] cmd;
	private boolean showOutput;
	private String[] envp = null;
	private File workDir = null;
	public JFrame launcher = null;

	public CmdRunner(String cmd, String[] envp, File workDir)
	{
		this(new String[]{cmd},envp,workDir);
	}
	public CmdRunner(String[] cmd, String[] envp, File workDir)
	{
		this(cmd,envp,workDir, false);
	}
	public CmdRunner(String cmd, String[] envp, File workDir, boolean showOutput)
	{
		this(new String[]{cmd},envp,workDir, showOutput);
	}

	public CmdRunner(String[] cmd, String[] envp, File workDir, boolean showOutput)
	{
		this.cmd = cmd;
		this.showOutput = showOutput;
		this.envp = envp;
		this.workDir = workDir;
	}

	public void run() throws IOException
	{
		Process p = null;
		try {
			if(cmd.length == 1)
				p = Runtime.getRuntime().exec(cmd[0], envp, workDir);
			else
				p = Runtime.getRuntime().exec(cmd, envp, workDir);
			if(this.showOutput)
			{
				ConsoleDialog cd = new ConsoleDialog(launcher, p, cmd);
				cd.show();
			}
			p.waitFor();
		} catch(InterruptedException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
}

package launcher;
import java.io.Serializable;

/*
 * Created on 20.05.2004
 * $Id: HZCharacter.java,v 1.3 2006/06/12 12:22:10 morghus-HorizonsLauncher Exp $
 */
/**
 * @author unknown
 */
public class HZCharacter implements Serializable, Comparable {
	
	private String IP;
	private String shardName;
	private String characterName;
	private int biote;
			
	/**
	 * @return Returns the characterName.
	 */
	public String getCharacterName() {
		return characterName;
	}
	/**
	 * @param characterName The characterName to set.
	 */
	public void setCharacterName(String characterName) {
		this.characterName = characterName;
	}
	/**
	 * @return Returns the server.
	 */

	public HZCharacter() {
	}
	/**
	 * @param ip
	 * @param shardName
	 * @param characterName
	 * @param biote
	 */
	public HZCharacter(String ip, String shardName, String characterName,
			int biote) {
		super();
		IP = ip;
		this.shardName = shardName;
		this.characterName = characterName;
		this.biote = biote;
	}
	/**
	 * @return Returns the biote.
	 */
	public int getBiote() {
		return biote;
	}
	/**
	 * @param biote The biote to set.
	 */
	public void setBiote(int biote) {
		this.biote = biote;
	}
	/**
	 * @return Returns the iP.
	 */
	public String getIP() {
		return IP;
	}
	/**
	 * @param ip The iP to set.
	 */
	public void setIP(String ip) {
		IP = ip;
	}
	/**
	 * @return Returns the shardName.
	 */
	public String getShardName() {
		return shardName;
	}
	/**
	 * @param shardName The shardName to set.
	 */
	public void setShardName(String shardName) {
		this.shardName = shardName;
	}
	
	public boolean equals(HZCharacter otherChar) {
		if (otherChar == null)
			return false;
		if (IP.equals(otherChar.getIP()) && 
				shardName.equals(otherChar.getShardName()) &&
				characterName.equals(otherChar.getCharacterName()) &&
				biote == otherChar.biote)
			return true;
		return false;
	}


	public int compareTo(Object o) {
		if (o.getClass() == String.class)
			return characterName.compareTo((String)o);
		else 
			return this.equals((HZCharacter)o) ? 0 : 1;
	}
}

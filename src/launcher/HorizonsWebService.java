package launcher;

import com.istaria.services.*;
import com.istaria.services.Character;
import launcher.serverStatus.ShardStatus;
import net.oceth.istaria.wsclient.IstariaWSClient;
import net.oceth.istaria.wsclient.IstariaWSException;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Implementation using the horizons webservice
 */
public class HorizonsWebService extends Observable implements HorizonsService {
    private IstariaWSClient client = null;
    public void login() throws Exception {
        try
        {
            notifyObservers("Logging in...");
            client = new IstariaWSClient(Configuration.getConfig().getHzAccount().getUsername(), Configuration.getConfig().getHzAccount().getPassword());
            if(!client.isLoggedIn())
            {
                client = null;
                notifyObservers("Login error!");
                throw new IstariaWSException("Error logging in!");
            }
            notifyObservers("Login complete");
        }
        catch(Exception e)
        {
            client = null;
            notifyObservers("Login error!");
            throw new IstariaWSException(e.getMessage(), e);
        }
    }

    public void logout() {
        client = null;
    }

    private String prepstr(String str){
        if(str != null)
        {
            str = str.trim();
        }
        return str;
    }


    public HZCharacter[] getCharacters() throws Exception {
        if(checkLogin())
        {
            notifyObservers("Loading characters...");
            Character[] characters = client.getCharacters();
            List<HZCharacter> ret = new ArrayList<HZCharacter>(characters.length);
            for (Character character : characters) {
                HZCharacter hzCharacter = new HZCharacter();
                hzCharacter.setBiote((int) character.getBiote_id());
                hzCharacter.setCharacterName(prepstr(character.getCharacter_name()));
                hzCharacter.setIP(prepstr(character.getIp_address()));
                hzCharacter.setShardName(prepstr(character.getShard()));
                ret.add(hzCharacter);
            }
            if(ret.size() > 0)
                return ret.toArray(new HZCharacter[ret.size()]);
        }
        return null;
    }

    private boolean checkLogin() {
        if(client == null)
        {
            try {
                login();
            } catch (Exception e) {
                notifyObservers("Login failed! "+e);
            }
        }
        return client != null;
    }

    public String getPublicKey() throws Exception {
        if(checkLogin())
        {
            notifyObservers("Retrieving public key...");
            return prepstr(client.getAccount().getPublic_key());
        }
        return null;
    }

    public String[] getShards() throws Exception {
        if(checkLogin())
        {
            notifyObservers("Retrieving shards...");
            return client.getShards();
        }
        return new String[0];
    }

    public HZCharacter getNewCharFor(String shardName) throws Exception {
        if(checkLogin())
        {
            notifyObservers("Retrieving new character data");
            CharReturnStruc character = client.newCharacter(shardName);
            HZCharacter hzCharacter = new HZCharacter();
            hzCharacter.setCharacterName("Player"+character.getBiote());
            hzCharacter.setBiote(character.getBiote());
            hzCharacter.setIP(prepstr(character.getIp()));
            hzCharacter.setShardName(prepstr(shardName));
            return hzCharacter;
        }
        return null;
    }

    public ShardStatus[] getShardStatus() throws Exception {
        if(checkLogin())
        {
            String[] status = client.getShards();
            List<ShardStatus> ret = new ArrayList<ShardStatus>(status.length);
            for (String s : status) {
                ShardStatus state = new ShardStatus(s, "?", "?");
                ret.add(state);
            }
            return ret.toArray(new ShardStatus[ret.size()]);
        }
        return new ShardStatus[0];
    }

    @Override
    public void notifyObservers(Object arg) {
        setChanged();
        super.notifyObservers(arg);
    }
}

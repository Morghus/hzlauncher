package launcher;
import java.io.BufferedInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Observable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import launcher.serverStatus.ShardStatus;

/*
 * Created on 24.05.2004
 * $Id: HorizonsWebsite.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
/**
 * @author unknown
 * I know the code sucks, but I wanted it fast, not nice
 */
public class HorizonsWebsite extends Observable implements HorizonsService {
	
	private boolean loggedIn;
	private String sessionId = null;
	private String userAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6) Gecko/20040206 Firefox/0.8";
	private String lastViewstate;

    /**
	 * 
	 */	
	public HorizonsWebsite() {
	}
	
	public void login() throws Exception {
		if (Configuration.getConfig().getHzAccount() == null)
			throw new Exception ("no username specified");
		login(Configuration.getConfig().getHzAccount().getUsername(), Configuration.getConfig().getHzAccount().getPassword());
	}
	
	public void logout() {
		Configuration.getConfig().setSessionId(null);
		this.setLoggedIn(false);
	}

	private void login(String user, String pass) throws Exception {
		URL url;
		HttpsURLConnection con;
		BufferedInputStream in = null;
		
		if (Configuration.getConfig().getSessionIdTime() != null && ((new Date().getTime() - Configuration.getConfig().getSessionIdTime().getTime()) / 1000) < 600L) {
			sessionId = Configuration.getConfig().getSessionId();
			loggedIn = true;
			return;
		}
		
		/* stage 1, getting viewstate */
		setChanged();
		notifyObservers(new String("Connecting (Stage 1)"));
		
		url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/default.aspx?c=");
		con = (HttpsURLConnection) url.openConnection();	
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", userAgent);
		con.connect();

		in = new BufferedInputStream(con.getInputStream());		
		
		int bytesRead;
		byte[] buf = new byte[2048];
		String tmp = "";
		while ((bytesRead = in.read(buf)) != -1) 
			tmp += new String(buf, 0, bytesRead);

		if (con.getHeaderField("Set-Cookie")!= null)
			setSessionIdFromCookie(con.getHeaderField("Set-Cookie"));
		
		con.disconnect();
		
		Matcher m = Pattern.compile("<input[^>]*name=\\\"__VIEWSTATE\\\"[^>]*value=\\\"([^\\\"]*)\\\"[^>]*\\/>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE).matcher(tmp);				
		if (m.find())
			lastViewstate = m.group(1);
		else
			throw new Exception("Could not initialize viewstate");
		
		/* stage 2, logging in and getting session id */
		setChanged();
		notifyObservers(new String("Connecting (Stage 2)"));		
		
		url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/default.aspx?c=");
		con = (HttpsURLConnection) url.openConnection();
		
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setInstanceFollowRedirects(false);
		
		con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/default.aspx?c=");
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setRequestProperty("User-Agent", userAgent);
		setSessionIdHeader(con);
		
		con.connect();

		String postRequest = "__VIEWSTATE=" + URLEncoder.encode(lastViewstate, "UTF-8") + "&user=" + URLEncoder.encode(user, "UTF-8") + "&pass=" + URLEncoder.encode(pass, "UTF-8") + "&submit=Login";
		con.getOutputStream().write(postRequest.getBytes());
		
		in = new BufferedInputStream(con.getInputStream());
		while ((bytesRead = in.read(buf)) != -1);
	
		if (con.getHeaderField("Set-Cookie")!= null)
			setSessionIdFromCookie(con.getHeaderField("Set-Cookie"));		
		
		con.disconnect();
		
		/* stage 3, getting viewstates and eula */
		setChanged();
		notifyObservers(new String("Connecting (Stage 3)"));
		
		url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/eula.aspx");
		con = (HttpsURLConnection) url.openConnection();
		
		con.setInstanceFollowRedirects(false);
		con.setRequestMethod("GET");
		
		con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/default.aspx");
		con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.6) Gecko/20040206 Firefox/0.8");
		setSessionIdHeader(con);

		con.connect();	

		in = new BufferedInputStream(con.getInputStream());
		tmp = "";
		while ((bytesRead = in.read(buf)) != -1)
			tmp += new String(buf, 0, bytesRead);

		if (con.getHeaderField("Set-Cookie")!= null)
			setSessionIdFromCookie(con.getHeaderField("Set-Cookie"));
		
		con.disconnect();
		
		m = Pattern.compile("<input[^>]*name=\\\"__VIEWSTATE\\\"[^>]*value=\\\"([^\\\"]*)\\\"[^>]*\\/>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE).matcher(tmp);				
		if (m.find())
			lastViewstate = m.group(1);
		else
			throw new Exception("Could not get new Viewstate. Possible Cause: Login failed");
		
		String eula = "";
		m = Pattern.compile("<textarea[^>]*>([^<]*)<\\/textarea>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE).matcher(tmp);				
		if (m.find())
			eula = m.group(1);
		else
			throw new Exception("Could not read EULA");
		
		/* stage 4, submitting eula and completing login */
		setChanged();
		notifyObservers(new String("Connecting (Stage 4)"));		
		
		url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/eula.aspx");
		con = (HttpsURLConnection) url.openConnection();	
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		con.setInstanceFollowRedirects(false);

		con.setRequestProperty("User-Agent", userAgent);
		con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/eula.aspx");
		setSessionIdHeader(con);
		
		con.connect();		

		postRequest = "__VIEWSTATE=" + URLEncoder.encode(lastViewstate, "UTF-8") + "&txtEula=" + URLEncoder.encode(eula, "UTF-8") + "&btnAgree=" + URLEncoder.encode("I Agree", "UTF-8");
		con.getOutputStream().write(((postRequest).getBytes()));
		
		in = new BufferedInputStream(con.getInputStream());
		while ((bytesRead = in.read(buf)) != -1);
		
		if (con.getHeaderField("Set-Cookie")!= null)
			setSessionIdFromCookie(con.getHeaderField("Set-Cookie"));

		con.disconnect();
		
		/* login done */
		setChanged();
		notifyObservers(new String("Login complete"));
		
		loggedIn = true;
		
		Configuration.getConfig().setSessionId(sessionId);
	}
	
	/**
	 * @param string
	 */
	private void setSessionIdFromCookie(String id) {
		this.sessionId = id.split("=")[1].split(";")[0];
	}
	
	private void setSessionIdHeader(HttpURLConnection con) {
		if (sessionId != null)
			con.setRequestProperty("Cookie", "ASP.NET_SessionId=" + sessionId);
	}
	
	public HZCharacter[] getCharacters() throws Exception {
		URL url; 
		HttpsURLConnection con;
		
		if (!loggedIn)
			login();
		
		setChanged();
		notifyObservers(new String("Loading Characters"));
		
		try {
			url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/default.aspx?c=");
			con = (HttpsURLConnection) url.openConnection();
			
			con.setRequestMethod("GET");
			con.setInstanceFollowRedirects(false);
	
			con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/eula.aspx");
			con.setRequestProperty("User-Agent", userAgent);
			setSessionIdHeader(con);
	
			con.connect();
			
			Configuration.getConfig().touchSessionIdTime();
		} catch (Exception e) {
			loggedIn = false;
			throw e;
		}
		
		BufferedInputStream in = new BufferedInputStream(con.getInputStream());
		String tmp = "";
		int bytesRead;
		byte[] buf = new byte[2048];
		while ((bytesRead = in.read(buf)) != -1)
			tmp += new String(buf, 0, bytesRead);		

		if (con.getHeaderField("Set-Cookie")!= null)
			setSessionIdFromCookie(con.getHeaderField("Set-Cookie"));

		con.disconnect();
		
		Matcher m = Pattern.compile("<a class=\\\"play\\\" href=\\\"([^\\\"]*)\\\">\\[SELECT\\]<\\/a>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE).matcher(tmp);
		LinkedList characters = new LinkedList();
		while (m.find()) {		
			Matcher mat = Pattern.compile("shard=(\\w*).*firstName=([^&]*).*biote=(\\d*).*IP=([0-9.:]*)").matcher(URLDecoder.decode(m.group(1), "UTF-8"));
			if (mat.find())
				characters.add(new HZCharacter(mat.group(4), mat.group(1), mat.group(2).trim(), Integer.parseInt(mat.group(3))));
		}

		if (characters.size() <= 0)
			return null;
		HZCharacter[] ret = new HZCharacter[characters.size()];
		Iterator it = characters.iterator();
		for (int i = 0; it.hasNext(); i++)
			ret[i] = (HZCharacter)it.next();
		return ret;
	}
	
	public String getPublicKey() throws Exception {
		URL url; 
		HttpsURLConnection con;
		
		if (!loggedIn)
			login();

		setChanged();
		notifyObservers(new String("Retrieving PublicKey"));
		
		try {		
			url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/Artifact/runGame.aspx?type=weblaunch_standard&shard=&firstName=&biote=&port=0&IP=");
			con = (HttpsURLConnection) url.openConnection();
			
			con.setRequestMethod("GET");
			con.setInstanceFollowRedirects(false);
	
			con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/default.aspx");
			con.setRequestProperty("User-Agent", userAgent);
			setSessionIdHeader(con);
	
			con.connect();
			Configuration.getConfig().touchSessionIdTime();
		} catch (Exception e) {
			loggedIn = false;
			throw e;
		}
		
		BufferedInputStream in = new BufferedInputStream(con.getInputStream());
		String tmp = "";
		int bytesRead;
		byte[] buf = new byte[2048];
		while ((bytesRead = in.read(buf)) != -1)
			tmp += new String(buf, 0, bytesRead);		

		if (con.getHeaderField("Set-Cookie")!= null)
			setSessionIdFromCookie(con.getHeaderField("Set-Cookie"));

		con.disconnect();
		
		String ret = "";
		
		Matcher m = Pattern.compile("publicKey = \\\"([^\\\"]*)\\\";").matcher(tmp);
		
		if (m.find())
			return m.group(1);
		else {
			loggedIn = false;
			throw new Exception("Could not get PublicKey. Possible Cause: No characters created or all servers offline.");
		}
	}
	
	public String[] getShards() throws Exception {
		URL url; 
		HttpsURLConnection con;

		if (!loggedIn)
			login();
		
		setChanged();
		notifyObservers(new String("Initializing login"));
		
		try {
			url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/default.aspx?c=");
			con = (HttpsURLConnection) url.openConnection();
	
			con.setRequestMethod("GET");
			con.setInstanceFollowRedirects(false);			
	
			con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/default.aspx");
			con.setRequestProperty("User-Agent", userAgent);
			setSessionIdHeader(con);
	
			con.connect();
			
			Configuration.getConfig().touchSessionIdTime();
		} catch (Exception e) {
			loggedIn = false;
			throw e;
		}

		BufferedInputStream in = new BufferedInputStream(con.getInputStream());
		String tmp = "";
		int bytesRead;
		byte[] buf = new byte[2048];
		while ((bytesRead = in.read(buf)) != -1)
			tmp += new String(buf, 0, bytesRead);
		
		con.disconnect();

		setChanged();
		notifyObservers(new String("Retrieving Shard List"));
		
		try {
			url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/characters/addcharacter.aspx");
			con = (HttpsURLConnection) url.openConnection();
	
			con.setRequestMethod("GET");
			con.setInstanceFollowRedirects(false);			
	
			con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/default.aspx");
			con.setRequestProperty("User-Agent", userAgent);
			setSessionIdHeader(con);
	
			con.connect();
			
			Configuration.getConfig().touchSessionIdTime();
		} catch (Exception e) {
			loggedIn = false;
			throw e;
		}

		in = new BufferedInputStream(con.getInputStream());
		tmp = "";
		buf = new byte[2048];
		while ((bytesRead = in.read(buf)) != -1)
			tmp += new String(buf, 0, bytesRead);
		
		con.disconnect();
		
		Matcher m = Pattern.compile("<input[^>]*name=\\\"__VIEWSTATE\\\"[^>]*value=\\\"([^\\\"]*)\\\"[^>]*\\/>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE).matcher(tmp);				
		if (m.find())
			lastViewstate = m.group(1);
		else {
			throw new Exception("Could not get new Viewstate. Possible cause: no shards online");
		}

		m = Pattern.compile("h\\.value = \\\"([^\\\"]*)\\\";").matcher(tmp);
		
		LinkedList shardList = new LinkedList();
		while (m.find()) {
			shardList.add(m.group(1));
		}
		
		if (shardList.size() <= 0) 
			throw new Exception("Could not find any online shards.");
		
		String[] ret = new String[shardList.size()];
		Iterator it = shardList.iterator();
		for (int i = 0; it.hasNext(); i++)
			ret[i] = (String)it.next();

		return ret;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public HZCharacter getNewCharFor(String shardName) throws Exception {
		URL url; 
		HttpsURLConnection con;

		if (!loggedIn)
			login();

		setChanged();
		notifyObservers(new String("Retrieving new character data"));
		
		try {
			url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/characters/addcharacter.aspx");
			con = (HttpsURLConnection) url.openConnection();
	
			con.setRequestMethod("POST");
			con.setInstanceFollowRedirects(false);
			con.setDoOutput(true);
			
			con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/characters/addcharacter.aspx");
			con.setRequestProperty("User-Agent", userAgent);
			setSessionIdHeader(con);
	
			con.connect();
		
			String postRequest = "__VIEWSTATE=" + URLEncoder.encode(lastViewstate, "UTF-8") + "&hidShardName=" + URLEncoder.encode(shardName, "UTF-8") + "&chk" + shardName + "=&btnCreate=" + URLEncoder.encode("Create", "UTF-8");
			con.getOutputStream().write(((postRequest).getBytes()));
			Configuration.getConfig().touchSessionIdTime();
		} catch (Exception e) {
			loggedIn = false;
			throw e;
		}

		BufferedInputStream in = new BufferedInputStream(con.getInputStream());
		String tmp = "";
		int bytesRead;
		byte[] buf = new byte[2048];
		while ((bytesRead = in.read(buf)) != -1)
			tmp += new String(buf, 0, bytesRead);
		
		String location = con.getHeaderField("Location");
		
		if (location == null)
			throw new Exception("Could not get new character information. Possible Cause: character limit reached or servers offline");
		
		con.disconnect();
		
		Matcher m = Pattern.compile("shard=(\\w*).*firstName=([A-Za-z-]*).*biote=(\\d*).*IP=([0-9.:]*)").matcher(URLDecoder.decode(location, "UTF-8"));
		if (m.find())
			return new HZCharacter(m.group(4), m.group(1), m.group(2), Integer.parseInt(m.group(3)));
		else
			throw new Exception("Could not get new characer information. Possible Cause: malformed URL");
	}
	
	public ShardStatus[] getShardStatus() throws Exception {
		URL url; 
		HttpsURLConnection con;

		if (!loggedIn)
			login();
		
		setChanged();
		notifyObservers(new String("Retrieving server status"));

		try {
			url = new URL("https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/default.aspx?c=");
			con = (HttpsURLConnection) url.openConnection();
	
			con.setRequestMethod("GET");
			con.setInstanceFollowRedirects(false);
	
			con.setRequestProperty("Referer", "https://" + Configuration.getConfig().getWebsiteURL() + "/horizons/default.aspx");
			con.setRequestProperty("User-Agent", userAgent);
			setSessionIdHeader(con);
	
			con.connect();
			Configuration.getConfig().touchSessionIdTime();
		} catch (Exception e) {
			loggedIn = false;
			throw e;
		}
		
		BufferedInputStream in = new BufferedInputStream(con.getInputStream());
		String tmp = "";
		int bytesRead;
		byte[] buf = new byte[2048];
		while ((bytesRead = in.read(buf)) != -1)
			tmp += new String(buf, 0, bytesRead);
		
		Matcher m = Pattern.compile("<table id=\"serverStatus\".*?>(.*?)</table>", Pattern.DOTALL | Pattern.CASE_INSENSITIVE).matcher(tmp);
		if (!m.find())
			throw new Exception("server status not found");
		
		String servers = m.group(1);
		// <td bgcolor=""><font color="#FFFFFF" size="1">Earth</font></td><td bgcolor=""><font color="#FFFFFF" size="1">Very Healthy</font></td><td bgcolor=""><font color="#FFFFFF" size="1">Very Light</font></td>
		
		m = Pattern.compile("<td.*?><font.*?>.*?</font></td><td.*?><font.*?>(.*?)</font></td><td.*?><font.*?>(.*?)</font></td><td.*?><font.*?>(.*?)</font></td>", Pattern.CASE_INSENSITIVE).matcher(servers);
		
		LinkedList shards = new LinkedList();
		while (m.find())
			shards.add(new ShardStatus(m.group(1), m.group(2), m.group(3)));
		
		ShardStatus[] ret = new ShardStatus[shards.size()];
		Iterator it = shards.iterator();
		for (int i = 0; it.hasNext(); i++) 
			ret[i] = (ShardStatus)it.next();
		
		return ret;
	}
}

package launcher;
import java.io.Serializable;

/*
 * Created on 20.05.2004
 * $Id: HZAccount.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
/**
 * @author ukw
 */
public class HZAccount implements Serializable {
	
	private String username;
	private String password;
	private String publicKey;
	
	/**
	 * @return Returns the publicKey.
	 */
	public String getPublicKey() {
		return publicKey;
	}
	/**
	 * @param publicKey The publicKey to set.
	 */
	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}
	/**
	 * @param username
	 * @param password
	 */
	public HZAccount(String username, String password) {
		this.username = username;
		this.password = password;
	}
	/**
	 * 
	 */
	public HZAccount() {
	}
	/**
	 * @return Returns the password.
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password The password to set.
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return Returns the username.
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username The username to set.
	 */
	public void setUsername(String username) {
		this.username = username;
	}
}

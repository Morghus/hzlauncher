package launcher;
import javax.swing.JLabel;

/*
 * Created on 24.05.2004
 * $Id: ProgressIcon.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */

/**
 * @author unknown
 */
public class ProgressIcon extends JLabel implements Runnable {

	/**
	 * 
	 */
	public ProgressIcon() {
		super();
		new Thread(this).start();
	}
	
	public void run() {
		String tmp = "";
		for (int i = 0; ;i++) {
			tmp = "";
			for (int j = 0; j < 6; j++)
				if ((i % 6) == j) 
					tmp += ".";
				else
					tmp += " ";
			this.setText(tmp);
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}		
	}
}

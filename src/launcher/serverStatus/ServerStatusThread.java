/*
 * Created on 17.07.2004
 * $Id: ServerStatusThread.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
package launcher.serverStatus;

import java.util.Observable;
import java.util.Observer;

import launcher.HorizonsService;

/**
 * @author unknown
 */
public class ServerStatusThread extends Observable implements Runnable, Observer {

	/**
	 * 
	 */
	public ServerStatusThread() {
		super();				
	}

	public void run() {
		try {
			HorizonsService.Factory.getInstance().addObserver(this);
			HorizonsService.Factory.getInstance().logout();
			ShardStatus[] status = HorizonsService.Factory.getInstance().getShardStatus();
			ServerStatusMessage msg = new ServerStatusMessage("", false, true);
			msg.setData(status);
			setChanged();
			notifyObservers(msg);			
		} catch (Exception e) {
			setChanged();
			notifyObservers(new ServerStatusMessage("Error: " + e.getMessage(), true));
		}
	}

	public void update(Observable o, Object arg) {
		setChanged();
		notifyObservers(new ServerStatusMessage((String)arg));
	}

}

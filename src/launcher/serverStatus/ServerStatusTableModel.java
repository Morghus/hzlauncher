/*
 * Created on 17.07.2004
 * $Id: ServerStatusTableModel.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
package launcher.serverStatus;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 * @author unknown
 */
public class ServerStatusTableModel extends AbstractTableModel implements Observer {

	private String columnNames[] = { "Shard Name", "Health", "Population" };
	private ServerStatusWindow parent;
	private ShardStatus[] data;
	private Thread dataThread;
	
	public ServerStatusTableModel(ServerStatusWindow parent) {
		super();
		this.parent = parent;
		getData();
	}
	
	public void getData() {
		ServerStatusThread thread = new ServerStatusThread();
		thread.addObserver(this);
		dataThread = new Thread(thread);
		dataThread.start();		
	}
	
	public int getColumnCount() {
		return columnNames.length;
	}
	
	public String getColumnName(int column) {
		return columnNames[column];
	}

	public int getRowCount() {
		if (data != null)
			return data.length;
		return 0;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
			case 0: return data[rowIndex].getShardName();
			case 1: return data[rowIndex].getShardHealth();
			case 2: return data[rowIndex].getShardPopulation();
			default: return null;
		}
	}

	public void update(Observable o, Object arg) {
		if (arg.getClass() == ServerStatusMessage.class) {
			ServerStatusMessage msg = (ServerStatusMessage)arg;
			if (!msg.getMessage().equals(""))
				parent.setStatusMessage(msg.getMessage());	
			if (msg.isDone()) {
				parent.setStatusMessage("");
				data = msg.getData();
				parent.updateTable();
			}
			if (msg.isError()) {
				parent.setStatusMessage("");
				JOptionPane.showMessageDialog(parent, msg.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				parent.setVisible(false);
			}
		}
	}
}

/*
 * Created on 17.07.2004
 * $Id: ShardStatus.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
package launcher.serverStatus;

/**
 * @author unknown
 */
public class ShardStatus {

	private String shardName = "";
	private String shardHealth = "";
	private String shardPopulation = "";
	
	public ShardStatus(String shardName, String shardHealth,
			String shardPopulation) {
		this.shardName = shardName;
		this.shardHealth = shardHealth;
		this.shardPopulation = shardPopulation;
	}
	
	public ShardStatus() {
	}

	/**
	 * @return Returns the shardHealth.
	 */
	public String getShardHealth() {
		return shardHealth;
	}
	/**
	 * @param shardHealth The shardHealth to set.
	 */
	public void setShardHealth(String shardHealth) {
		this.shardHealth = shardHealth;
	}
	/**
	 * @return Returns the shardName.
	 */
	public String getShardName() {
		return shardName;
	}
	/**
	 * @param shardName The shardName to set.
	 */
	public void setShardName(String shardName) {
		this.shardName = shardName;
	}
	/**
	 * @return Returns the shardPopulation.
	 */
	public String getShardPopulation() {
		return shardPopulation;
	}
	/**
	 * @param shardPopulation The shardPopulation to set.
	 */
	public void setShardPopulation(String shardPopulation) {
		this.shardPopulation = shardPopulation;
	}
}

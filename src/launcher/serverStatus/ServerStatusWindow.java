package launcher.serverStatus;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import launcher.HorizonsLauncher;
import launcher.ProgressDialog;
/*
 * Created on 05.07.2004
 * $Id: ServerStatusWindow.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */

/**
 * @author unknown
 */
public class ServerStatusWindow extends JDialog {

	protected JTable table;
	protected ProgressDialog dlg;
	
	public ServerStatusWindow(HorizonsLauncher parent) {
		super(parent, "Horizons Launcher - Server Status", true);
		
		WindowListener listener = new WindowListener() {

			public void windowActivated(WindowEvent e) {}
			public void windowClosed(WindowEvent e) {}
			public void windowClosing(WindowEvent e) {
				setVisible(false);
			}
			public void windowDeactivated(WindowEvent e) {}
			public void windowDeiconified(WindowEvent e) {}
			public void windowIconified(WindowEvent e) {}
			public void windowOpened(WindowEvent e) {}			
		};
		
		ActionListener closeListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		};
		
		ActionListener refreshListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((ServerStatusTableModel)table.getModel()).getData();
				dlg.setVisible(true);
			}
		};
		
		setSize(320, 150);
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		dlg = new ProgressDialog(parent, "Server Status...");
				
		addWindowListener(listener);
		
		table = new JTable(new ServerStatusTableModel(this));
		JScrollPane pane = new JScrollPane(table);
		
		Container cont = getContentPane();
		cont.setLayout(new BorderLayout());
		
		Container southCont = new Container();
		southCont.setLayout(new FlowLayout());

		JButton refreshButton = new JButton("Refresh");
		refreshButton.addActionListener(refreshListener);
		southCont.add(refreshButton);

		JButton closeButton = new JButton("Close");
		closeButton.addActionListener(closeListener);
		southCont.add(closeButton);
		
		cont.add(pane, BorderLayout.CENTER);
		cont.add(southCont, BorderLayout.SOUTH);
		
		dlg.setVisible(true);

		setVisible(true);
	}


	public void setStatusMessage(String message) {
		if (message.equals(""))
			dlg.setVisible(false);
		dlg.setText(message);
	}

	public void updateTable() {
		table.updateUI();
	}
}

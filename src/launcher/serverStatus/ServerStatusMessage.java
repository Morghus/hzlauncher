/*
 * Created on 18.07.2004
 * $Id: ServerStatusMessage.java,v 1.2 2004/09/12 14:08:20 morghus-HorizonsLauncher Exp $
 */
package launcher.serverStatus;

import launcher.ObserverMessage;

/**
 * @author unknown
 */
public class ServerStatusMessage extends ObserverMessage {

	private ShardStatus[] data = null;
	
	/**
	 * @param message
	 */
	public ServerStatusMessage(String message) {
		super(message);
	}

	/**
	 * @param message
	 * @param error
	 */
	public ServerStatusMessage(String message, boolean error) {
		super(message, error);
	}

	/**
	 * @param message
	 * @param error
	 * @param done
	 */
	public ServerStatusMessage(String message, boolean error, boolean done) {
		super(message, error, done);
	}

	/**
	 * @return Returns the data.
	 */
	public ShardStatus[] getData() {
		return data;
	}
	/**
	 * @param data The data to set.
	 */
	public void setData(ShardStatus[] data) {
		this.data = data;
	}
}

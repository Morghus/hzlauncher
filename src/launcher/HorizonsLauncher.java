package launcher;

import launcher.charactersUpdate.CreateCharacterDialog;
import launcher.charactersUpdate.UpdateCharactersMessage;
import launcher.charactersUpdate.UpdateCharactersThread;
import launcher.linuxConfig.LinuxConfigDialog;
import launcher.patcher.PatcherWindow;
import launcher.serverStatus.ServerStatusWindow;

import javax.net.ssl.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Observable;
import java.util.Observer;

import static launcher.util.BrowserLaunchHelper.makeKlickable;
import launcher.util.VersionCheck;


/*
 * Created on 20.05.2004
 * $Id: HorizonsLauncher.java,v 1.19 2006/06/16 23:45:53 morghus-HorizonsLauncher Exp $
 */
/**
 * @author unknown
 */
public class HorizonsLauncher extends JFrame implements Observer {

	/**
	 *
	 */
	private static final long serialVersionUID = -534561327609114060L;

	private JComboBox characterSelection;
	
	/* dialog */
	private JCheckBox crcCheck;
	private JCheckBox listDownload;
	private JTextField gamePathLabel;
	private JTextField emailText;
	private JPasswordField passwordText;
	private JTextField publicKeyText;
	
	private ProgressDialog updateDialog;
	private JButton startButton;
	private JCheckBox fullScanBox;
	private ButtonGroup accountTypeGroup;

	private static final String VERSION = "1.6.5";

	private JCheckBox fileSize;
	private JCheckBox keepFiles;
	private JCheckBox patchShaders;
	private JCheckBox enableConsole;
	private JCheckBox enableVersionCheck;
    private JCheckBox useLegacyService;

	public static void main(String[] args)  {
        try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		TrustManager[] tm = { new X509TrustManager() {
			public boolean isClientTrusted (X509Certificate[] cert) { return true; }
			public boolean isServerTrusted (X509Certificate[] cert) { return true; }
			public X509Certificate[] getAcceptedIssuers() { return null; }
			public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}
			public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {}
		}};

		try {
			SSLContext sslc = SSLContext.getInstance("TLS");
			sslc.init(null, tm, new SecureRandom());
			SSLSocketFactory sslsf = sslc.getSocketFactory();
			HttpsURLConnection.setDefaultSSLSocketFactory(sslsf);
		} catch(NoSuchAlgorithmException nsaex) {
		} catch(KeyManagementException kmex) { }

		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String arg0, SSLSession arg1) {
				return true;
			}
		});

		new HorizonsLauncher();
	}
	
	public HorizonsLauncher() {
		super("Horizons Launcher");
		Configuration.loadConfig();

        Toolkit.getDefaultToolkit().setDynamicLayout(true);        
		
		WindowListener windowListener = new WindowListener() {
			public void windowClosing(WindowEvent arg0) {
				Configuration.saveConfig();
				System.exit(0);
			}
			public void windowActivated(WindowEvent arg0) {}
			public void windowClosed(WindowEvent arg0) {}
			public void windowDeactivated(WindowEvent arg0) {}
			public void windowDeiconified(WindowEvent arg0) {}
			public void windowIconified(WindowEvent arg0) {}
			public void windowOpened(WindowEvent arg0) {}		
		};
		
		ActionListener startListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				startHorizons(characterSelection.getSelectedIndex());
			}
		};
		
		Image icon = new ImageIcon(getClass().getResource("/horizons.png")).getImage();
		
		addWindowListener(windowListener);
		setResizable(false);
		setIconImage(icon);
		
		Container mainContent = getContentPane();
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(2, 2, 2, 2);

		mainContent.setLayout(gridBag);	
		
		JLabel characterLabel = new JLabel("Character");
		c.weightx = 0;
		c.anchor = GridBagConstraints.EAST;
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(characterLabel, c);
		mainContent.add(characterLabel);

		characterSelection = new JComboBox();
		characterSelection.setPreferredSize(new Dimension(300, 20));
		setComboBoxCharacters(characterSelection);
		c.weightx = 1.0;
		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(characterSelection, c);
		mainContent.add(characterSelection);
		
		fullScanBox = new JCheckBox("perform Full Scan");
		c.weightx = 1.0;
		gridBag.setConstraints(fullScanBox, c);
		mainContent.add(fullScanBox);
				
		startButton = new JButton("Start Horizons");
		startButton.addActionListener(startListener);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		gridBag.setConstraints(startButton, c);
		mainContent.add(startButton);
		checkStartButton();

		buildMenu();
		
		pack();
		setLocationRelativeTo(null);

		if(Configuration.getConfig().isEnableVersionCheck())
		{
			new VersionCheck(this, VERSION).check();
		}
		
		setVisible(true);
	}
	
	private void checkStartButton() {
		if (Configuration.getConfig().getHzCharacters() == null)
			startButton.setEnabled(false);
		else
			startButton.setEnabled(true);
	}

	public void startHorizons(HZCharacter hzChar) {
		HorizonsLauncherStrategy.Factory.newInstance().startHorizons(hzChar, fullScanBox.isSelected(), this);
	}

	protected void startHorizons(int i) {
		HZCharacter hzChar = Configuration.getConfig().getHzCharacters()[i];
		startHorizons(hzChar);
	}

	private void setComboBoxCharacters(JComboBox box) {
		HZCharacter[] chars = Configuration.getConfig().getHzCharacters();
		box.setEnabled(true);
		box.removeAllItems();
		if (chars == null) {
			box.addItem("NO CHARACTERS LOADED");
			box.setEnabled(false);
			return;
		}
		for (int i = 0; i < chars.length; i++) {
			box.addItem("\"" + chars[i].getCharacterName() + "\" on " + chars[i].getShardName());
			if (chars[i].equals(Configuration.getConfig().getLastChar()))
				box.setSelectedIndex(i);
		}
		return;
	}

	private void buildMenu() {
		
		ActionListener preferencesListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				showPreferencesDialog();
			}		
		};
		
		ActionListener exitListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Configuration.saveConfig();
				System.exit(0);
			}		
		};
	
		ActionListener createCharListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Configuration.getConfig().getHzAccount() == null)
					JOptionPane.showMessageDialog(getFrame(), "Please set the account information first.", "Error", JOptionPane.ERROR_MESSAGE);
				else
					createCharacter();
			}
		};
		
		ActionListener runPatchListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				openSelectShardPatchWindow();
			}
		};
		
		ActionListener shardStatusListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ServerStatusWindow(getFrame());
			}
		};
		
		ActionListener aboutListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showAboutDialog();
			}
		};		
		
		JMenuBar menuBar = new JMenuBar();
		
		JMenu menu = new JMenu("File");
		menu.setMnemonic('F');
		
		JMenuItem item = new JMenuItem("Preferences...");		
		item.setMnemonic('P');
		item.addActionListener(preferencesListener);
		menu.add(item);
		
		menu.addSeparator();

		item = new JMenuItem("Create new character...");		
		item.setMnemonic('C');
		item.addActionListener(createCharListener);
		menu.add(item);
		
		item = new JMenuItem("Run full scan & patch...");		
		item.setMnemonic('R');
		item.addActionListener(runPatchListener);
		menu.add(item);
		
		item = new JMenuItem("Show Shard Status...");		
		item.setMnemonic('S');
		item.addActionListener(shardStatusListener);
		menu.add(item);
		
		menu.addSeparator();
		
		item = new JMenuItem("Exit");
		item.setMnemonic('E');
		item.addActionListener(exitListener);		
		menu.add(item);
		
		menuBar.add(menu);	
		
		menu = new JMenu("?");
		menu.setMnemonic('?');
		
		item = new JMenuItem("About");
		item.setMnemonic('A');
		item.addActionListener(aboutListener);
		menu.add(item);
		
		menuBar.add(menu);	
		
		setJMenuBar(menuBar);
	}

	/**
	 * @return
	 */
	protected void openSelectShardPatchWindow() {
		SelectShardWindow wnd = new SelectShardWindow(this);
		if (wnd.getSelectedShard() == null)
			return;
		PatcherWindow patcherWindow = new PatcherWindow(getFrame());
		patcherWindow.runPatch(true, wnd.getSelectedShard());
		if (!patcherWindow.isError())
			JOptionPane.showMessageDialog(getFrame(), "Patching completed successfully!", "Information", JOptionPane.INFORMATION_MESSAGE);
		else
			JOptionPane.showMessageDialog(getFrame(), "Patching aborted!", "Error", JOptionPane.ERROR_MESSAGE);			
	}

	/**
	 * 
	 */
	protected void showAboutDialog() {
		final JDialog about = new JDialog(this, "About Horizons Launcher", true);

		ActionListener okListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				about.setVisible(false);
			}			
		};
		
		Container cont = about.getContentPane();
		cont.setLayout(new GridLayout(0,1));
		
		JLabel launcherLabel = new JLabel("Horizons Launcher\n");
		launcherLabel.setFont(new Font("Tahoma", Font.BOLD, 14));

		JButton okButton = new JButton("OK");
		okButton.addActionListener(okListener);
		
		cont.add(launcherLabel);
		cont.add(new JLabel("Version " + VERSION));
		cont.add(makeKlickable("http://horizons.oceth.net/",new JLabel("<html><body><a href=\"http://horizons.oceth.net/\">http://horizons.oceth.net/</a></body></html>")));
		cont.add(new JLabel("http://www.the-first-born.org/"));
		cont.add(makeKlickable("http://www.menc2k.de/",new JLabel("<html><body><a href=\"http://www.menc2k.de/\">http://www.menc2k.de/</a></body></html>")));
		cont.add(new JLabel("USE AT YOUR OWN RISK!\n"));
		cont.add(okButton);
		
		about.setSize(240, 180);
		about.setLocationRelativeTo(this);
		about.setResizable(false);
		about.setVisible(true);
	}

	protected void createCharacter() {
		new CreateCharacterDialog(this);
	}

	protected void showPreferencesDialog() {
		final JDialog dlg = new JDialog(this);
		dlg.setTitle("Horizons Launcher Preferences");
		dlg.setModal(true);
		dlg.setResizable(false);
			
		ActionListener browseAction = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser;
				if (gamePathLabel.getText() != null)
					fileChooser = new JFileChooser(gamePathLabel.getText());
				else
					fileChooser = new JFileChooser();
				fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				fileChooser.showOpenDialog(getFrame());
				File selectedFile = fileChooser.getSelectedFile();
				if (selectedFile == null)
					return;
				gamePathLabel.setText(selectedFile.getPath());				
			}
		};	
		
		ActionListener cancelAction = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				checkStartButton();
				setComboBoxCharacters(characterSelection);
				characterSelection.updateUI();
				dlg.setVisible(false);
			}
		};	
		
		ActionListener okAction = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!new File(gamePathLabel.getText()).isDirectory())
				{
					JOptionPane.showMessageDialog(getFrame(), "Horizons Path doesn't exist!", "Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
					
				Configuration.getConfig().setPerformCRCcheck(crcCheck.isSelected());
				Configuration.getConfig().setDisableGetFileSizes(!fileSize.isSelected());
				Configuration.getConfig().setHorizonsPath(gamePathLabel.getText());
				Configuration.getConfig().setKeepNewerFiles(keepFiles.isSelected());
				Configuration.getConfig().setEnableVersionCheck(enableVersionCheck.isSelected());
				Configuration.getConfig().setUseLegacyServiceImpl(useLegacyService.isSelected());

				if (Configuration.getConfig().getHzAccount() == null)
					Configuration.getConfig().setHzAccount(new HZAccount());				
				
				if (Configuration.getConfig().getHzAccount().getUsername() != null && Configuration.getConfig().getHzAccount().getPassword() != null)
					if (!Configuration.getConfig().getHzAccount().getUsername().equals(emailText.getText()) || !Configuration.getConfig().getHzAccount().getPassword().equals(new String(passwordText.getPassword())))
						HorizonsService.Factory.getInstance().logout();
				
				Configuration.getConfig().getHzAccount().setUsername(emailText.getText());
				Configuration.getConfig().getHzAccount().setPassword(new String(passwordText.getPassword()));
				
				if (!Configuration.getConfig().getWebsiteURL().equals("play.istaria.com"))
					HorizonsService.Factory.getInstance().logout();
				Configuration.getConfig().setPatchURL("patch.istaria.com");
				Configuration.getConfig().setWebsiteURL("play.istaria.com");

				
				if(HorizonsLauncherStrategy.Factory.isLinux())
				{
					Configuration.getConfig().setPatchShaderFiles(patchShaders.isSelected());
					Configuration.getConfig().setEnableDebugConsole(enableConsole.isSelected());
				}
				else
				{
					Configuration.getConfig().setPatchShaderFiles(false);
					Configuration.getConfig().setEnableDebugConsole(false);
				}
				
				Configuration.saveConfig();
				
				checkStartButton();
				setComboBoxCharacters(characterSelection);
				
				characterSelection.updateUI();
				dlg.setVisible(false);
			}
		};
		ActionListener updateListener = new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Configuration.getConfig().getHzAccount() == null)
					Configuration.getConfig().setHzAccount(new HZAccount());
				
				if (Configuration.getConfig().getHzAccount().getUsername() != null && Configuration.getConfig().getHzAccount().getPassword() != null)
					if (!Configuration.getConfig().getHzAccount().getUsername().equals(emailText.getText()) || !Configuration.getConfig().getHzAccount().getPassword().equals(new String(passwordText.getPassword())))
						HorizonsService.Factory.getInstance().logout();
				
				Configuration.getConfig().getHzAccount().setUsername(emailText.getText());
				Configuration.getConfig().getHzAccount().setPassword(new String(passwordText.getPassword()));			

				if (!Configuration.getConfig().getWebsiteURL().equals("play.istaria.com"))
					HorizonsService.Factory.getInstance().logout();
				Configuration.getConfig().setPatchURL("patch.istaria.com");
				Configuration.getConfig().setWebsiteURL("play.istaria.com");

				runUpdateCharacters();
			}
		};
		
		GridBagLayout gridBag = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		
		Container cont = dlg.getContentPane();
		cont.setLayout(gridBag);
		
		/* patch configuration */
		JPanel patchConfiguration = new JPanel();
		patchConfiguration.setBorder(BorderFactory.createTitledBorder("Patch Configuration"));
		GridBagLayout gridBagPatch = new GridBagLayout();
		patchConfiguration.setLayout(gridBagPatch);
		
		crcCheck = new JCheckBox("do CRC check when updating files (slower)");
		crcCheck.setSelected(Configuration.getConfig().isPerformCRCcheck());
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBagPatch.setConstraints(crcCheck, c);
		patchConfiguration.add(crcCheck);
		
		fileSize = new JCheckBox("<html><body>get real file sizes when downloading files<br>(fetches sizes from server in background)</body></html>");
		fileSize.setSelected(!Configuration.getConfig().isDisableGetFileSizes());
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBagPatch.setConstraints(fileSize, c);
		patchConfiguration.add(fileSize);		
		
		keepFiles = new JCheckBox("keep files if newer than repository");
		keepFiles.setSelected(Configuration.getConfig().isKeepNewerFiles());
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBagPatch.setConstraints(keepFiles, c);
		patchConfiguration.add(keepFiles);		

		/* game configuration */
		JPanel gameConfiguration = new JPanel();
		gameConfiguration.setBorder(BorderFactory.createTitledBorder("Horizons Path"));
		GridBagLayout gridBagGame = new GridBagLayout();
		gameConfiguration.setLayout(gridBagGame);

		String path;
		if (Configuration.getConfig().getHorizonsPath() != null)
			path = Configuration.getConfig().getHorizonsPath();
		else
			path = "";
		gamePathLabel = new JTextField(path);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBagGame.setConstraints(gamePathLabel, c);
		gameConfiguration.add(gamePathLabel);
		
		JButton browseButton = new JButton("Browse...");
		c.fill = GridBagConstraints.NONE;
		c.weightx = 0.0;
		browseButton.addActionListener(browseAction);
		gridBagGame.setConstraints(browseButton, c);
		gameConfiguration.add(browseButton);
		
		/* generic configuration */
		JPanel genericConfiguration = new JPanel();
		genericConfiguration.setBorder(BorderFactory.createTitledBorder("Other"));
		GridBagLayout gridBagGeneric = new GridBagLayout();
		genericConfiguration.setLayout(gridBagGeneric);


		enableVersionCheck = new JCheckBox("check for new version at startup");
		enableVersionCheck.setSelected(Configuration.getConfig().isEnableVersionCheck());
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBagGeneric.setConstraints(enableVersionCheck, c);
		genericConfiguration.add(enableVersionCheck);

		useLegacyService = new JCheckBox("use legacy login service");
		useLegacyService.setSelected(Configuration.getConfig().isUseLegacyServiceImpl());
		gridBagGeneric.setConstraints(useLegacyService, c);
		genericConfiguration.add(useLegacyService);

		/* Account Configuration */
		c.fill = GridBagConstraints.NONE;
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.weightx = 0.0;
		JPanel acctConfiguration = new JPanel();
		acctConfiguration.setBorder(BorderFactory.createTitledBorder("Horizons Account"));
		GridBagLayout gridBagAcct = new GridBagLayout();
		acctConfiguration.setLayout(gridBagAcct);
		c.insets = new Insets(1,1,1,1);
		
		JLabel emailLabel = new JLabel("E-Mail Address: ");
		c.fill = GridBagConstraints.NONE;
		c.weightx = 0;
		gridBagAcct.setConstraints(emailLabel, c);
		acctConfiguration.add(emailLabel);
		
		emailText = new JTextField();
		if (Configuration.getConfig().getHzAccount() != null && Configuration.getConfig().getHzAccount().getUsername() != null)
			emailText.setText(Configuration.getConfig().getHzAccount().getUsername());
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBagAcct.setConstraints(emailText, c);
		acctConfiguration.add(emailText);
		
		JLabel passwordLabel = new JLabel("Password: ");
		c.fill = GridBagConstraints.NONE;
		c.gridwidth = GridBagConstraints.RELATIVE;
		c.weightx = 0;
		gridBagAcct.setConstraints(passwordLabel, c);
		acctConfiguration.add(passwordLabel);		
		
		passwordText = new JPasswordField();
		if (Configuration.getConfig().getHzAccount() != null && Configuration.getConfig().getHzAccount().getUsername() != null)
			passwordText.setText(Configuration.getConfig().getHzAccount().getPassword());
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBagAcct.setConstraints(passwordText, c);
		acctConfiguration.add(passwordText);	
		
		JButton updateCharactersButton = new JButton("Update Characters");
		updateCharactersButton.addActionListener(updateListener);
		c.fill = GridBagConstraints.NONE;
		c.weightx = 0;
		c.anchor = GridBagConstraints.CENTER;
		gridBagAcct.setConstraints(updateCharactersButton, c);
		acctConfiguration.add(updateCharactersButton);		

		/* main dlg */
		c.insets = new Insets(0,0,0,0);
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1.0;
		c.gridwidth = GridBagConstraints.REMAINDER;
		gridBag.setConstraints(patchConfiguration, c);
		cont.add(patchConfiguration);
		gridBag.setConstraints(gameConfiguration, c);
		cont.add(gameConfiguration);
		
		if(HorizonsLauncherStrategy.Factory.isLinux())
		{
			JPanel linuxCfg = new JPanel();
			GridBagLayout gbl = new GridBagLayout();
			GridBagConstraints gbc = new GridBagConstraints();
			linuxCfg.setLayout(gbl);
			linuxCfg.setBorder(BorderFactory.createTitledBorder("Linux specific settings"));

			gbc.fill = GridBagConstraints.BOTH;
			gbc.gridwidth = GridBagConstraints.REMAINDER;


			patchShaders = new JCheckBox("Apply addsmooth patch to shaders (recommended)");
			patchShaders.setSelected(Configuration.getConfig().isPatchShaderFiles());
			gbl.setConstraints(patchShaders,gbc);
			linuxCfg.add(patchShaders);
			
			enableConsole = new JCheckBox("Enable debug console");
			enableConsole.setSelected(Configuration.getConfig().isEnableDebugConsole());
			gbl.setConstraints(enableConsole,gbc);
			linuxCfg.add(enableConsole);

			JButton emulatorCfg = new JButton("Emulator configuration");
			emulatorCfg.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					LinuxConfigDialog lcd = new LinuxConfigDialog(dlg);
					lcd.setLocationRelativeTo(dlg);
					lcd.setSize(dlg.getSize());
					//lcd.pack();
					lcd.show();
				}
			});
			gbl.setConstraints(emulatorCfg,gbc);
			linuxCfg.add(emulatorCfg);
			
			gridBag.setConstraints(linuxCfg,c);
			cont.add(linuxCfg);
		}
		else
		{
			Configuration.getConfig().setPatchShaderFiles(false); // do not patch the files in windows
		}
		
		gridBag.setConstraints(acctConfiguration, c);
		cont.add(acctConfiguration);
		gridBag.setConstraints(genericConfiguration, c);
		cont.add(genericConfiguration);

		
		
		JButton okButton = new JButton("OK");
		okButton.addActionListener(okAction);
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(cancelAction);
		c.insets = new Insets(2,2,2,2);
		c.fill = GridBagConstraints.NONE;
		c.weightx = 1;
		c.anchor = GridBagConstraints.EAST;
		c.gridwidth = GridBagConstraints.RELATIVE;
		gridBag.setConstraints(okButton, c);
		cont.add(okButton);
		c.anchor = GridBagConstraints.WEST;
		gridBag.setConstraints(cancelButton, c);
		cont.add(cancelButton);
		
		dlg.pack();
		dlg.setLocationRelativeTo(this);		
				
		dlg.setVisible(true);
	}

	protected void runUpdateCharacters() {
		updateDialog = new ProgressDialog(this, "Updating Characters...");
				
		UpdateCharactersThread thread = new UpdateCharactersThread();
		thread.addObserver(this);
		new Thread(thread).start();
		
		updateDialog.setVisible(true);
	}
	
	public HorizonsLauncher getFrame() {
		return this;
	}

	public void update(Observable obs, Object arg) {
		if (obs.getClass() == UpdateCharactersThread.class) {
			UpdateCharactersMessage message = (UpdateCharactersMessage) arg;
			
			if (message.isDone() || message.isError())
				updateDialog.setVisible(false);
			
			if (message.isDone())
				JOptionPane.showMessageDialog(this, "Characters have been updated from the website.\nIf you're missing some characters, the server they're on may be offline.", "Information", JOptionPane.INFORMATION_MESSAGE);
			else if (message.isError())
				JOptionPane.showMessageDialog(this, "Error: " + message.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			else
				updateDialog.setText(message.getMessage());			
		}
	}
}

package launcher;

import launcher.linuxConfig.CedegaConfig;
import launcher.linuxConfig.WineConfig;
import launcher.linuxConfig.Cedega52Config;
import launcher.strategyImpl.CedegaLauncherStrategyImpl;
import launcher.strategyImpl.WindowsHZLauncherStrategyImpl;
import launcher.strategyImpl.WineLauncherStrategyImpl;
import launcher.strategyImpl.Cedega52LauncherStrategyImpl;

import javax.swing.*;
import java.io.File;

/**
 * @author oceth
 */
public interface HorizonsLauncherStrategy {
	public void startHorizons(HZCharacter hzChar, boolean fullScan, JFrame launcherWindow);

	static class Factory
	{
		public static HorizonsLauncherStrategy newInstance() {
			if(isLinux())
			{
				if(Configuration.getConfig().getLinuxConfig() instanceof CedegaConfig)
					return new CedegaLauncherStrategyImpl();
				else if(Configuration.getConfig().getLinuxConfig() instanceof Cedega52Config)
					return new Cedega52LauncherStrategyImpl();
				else if(Configuration.getConfig().getLinuxConfig() instanceof WineConfig)
					return new WineLauncherStrategyImpl();
				/*
				else
					return new LinuxHZLauncherStrategyImpl();
				*/
			}
			return new WindowsHZLauncherStrategyImpl();
		}

		public static boolean isLinux() {
			if(System.getProperty("os.name").indexOf("Linux") != -1)
				return true;
			File lnx = new File(System.getProperty("user.home")+File.separator+"HZLauncherForceLinuxMode");
			if(lnx.exists())
				return true;
			return false;
		}
	}
}

package launcher;

import launcher.serverStatus.ShardStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Observer;

/**
 * Created by IntelliJ IDEA.
 * User: cpph
 * Date: Nov 30, 2010
 * Time: 11:25:23 AM
 * To change this template use File | Settings | File Templates.
 */
public interface HorizonsService {

    void login() throws Exception;

    void logout();

    HZCharacter[] getCharacters() throws Exception;

    String getPublicKey() throws Exception;

    String[] getShards() throws Exception;

    HZCharacter getNewCharFor(String shardName) throws Exception;

    ShardStatus[] getShardStatus() throws Exception;

    public void addObserver(Observer observer);

    public static class Factory {
        private static Log log = LogFactory.getLog(HorizonsService.class);

        private static HorizonsService site = null;

        public static HorizonsService getInstance() {

            if(!Configuration.getConfig().isUseLegacyServiceImpl()
               && (site == null || (site instanceof HorizonsWebsite)))
            {
                // Try to load it using weak binding, assuring it works...
                try {
                    site = (HorizonsService) HorizonsService.class.getClassLoader()
                            .loadClass("launcher.HorizonsWebService").newInstance();
                } catch (Exception e) {
                    log.error("Error instancing WebService client!",e);
                    if(!(site instanceof HorizonsWebsite))
                    {
                        site = null;
                    }
                }
            }
            if(site == null || (Configuration.getConfig().isUseLegacyServiceImpl()
                                && !(site instanceof HorizonsWebsite)))
            {
                Configuration.getConfig().setUseLegacyServiceImpl(true);
                site = new HorizonsWebsite();
            }

            return site;
        }
    }
}

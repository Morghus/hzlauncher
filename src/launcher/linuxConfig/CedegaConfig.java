package launcher.linuxConfig;

import java.io.File;
import java.util.List;
import java.util.ArrayList;

/**
 * @author Oceth
 */
public class CedegaConfig implements LinuxConfig {
	static final long serialVersionUID = 120060612L;

	private String cedegaPath = "/usr/bin/cedega";
	private String cedegaConfig = "Default";
	private String game = "Default";
	private String winVer = "Default";
	private String debugMsgs = "";

	public String getCedegaPath() {
		return cedegaPath;
	}

	public void setCedegaPath(String cedegaPath) {
		this.cedegaPath = cedegaPath;
	}

	public String getCedegaConfig() {
		return cedegaConfig;
	}

	public void setCedegaConfig(String cedegaConfig) {
		this.cedegaConfig = cedegaConfig;
	}

	public String getGame() {
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public String getWinVer() {
		return winVer;
	}

	public void setWinVer(String winVer) {
		this.winVer = winVer;
	}

	public String getDebugMsgs() {
		return debugMsgs;
	}

	public void setDebugMsgs(String debugMsgs) {
		this.debugMsgs = debugMsgs;
	}

	public String[] getWindowsEmulationModes() {
		return new String[] {"Default", "win95", "win98", "nt40", "win351", "winme", "win2k", "winxp"};
	}

	public String[] getCedegaConfigurations() {
		File cfgProf = new File(System.getProperty("user.home") + File.separator + ".cedega" + File.separator +"configuration_profiles");
		if(cfgProf.exists() && cfgProf.isDirectory())
		{
			List profiles = new ArrayList();
			profiles.add("Default");
			File[] files = cfgProf.listFiles();
			for(int i=0; i < files.length; ++i)
			{
				if(files[i].isFile() && !files[i].isHidden())
				{
					profiles.add(files[i].getName());
				}
			}
			return (String[]) profiles.toArray(new String[profiles.size()]);
		}
		return new String[]{"Default"};
	}

	public String[] getGames() {
		File cfgProf = new File(System.getProperty("user.home") + File.separator + ".cedega");
		if(cfgProf.exists() && cfgProf.isDirectory())
		{
			List profiles = new ArrayList();
			profiles.add("Default");
			File[] files = cfgProf.listFiles();
			for(int i=0; i < files.length; ++i)
			{
				if(files[i].isDirectory() && !files[i].isHidden() && !files[i].getName().equals("configuration_profiles"))
				{
					profiles.add(files[i].getName());
				}
			}
			return (String[]) profiles.toArray(new String[profiles.size()]);
		}
		return new String[]{"Default"};
	}

	public CedegaConfig copy() {
		CedegaConfig ret = new CedegaConfig();
		ret.setCedegaPath(cedegaPath);
		ret.setCedegaConfig(cedegaConfig);
		ret.setGame(game);
		ret.setWinVer(winVer);
		ret.setDebugMsgs(debugMsgs);
		return ret;
	}
}

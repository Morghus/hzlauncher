package launcher.linuxConfig;

import java.io.Serializable;

/**
 * @author cpph
*/
public class CedegaGameIcon implements Serializable {
	static final long serialVersionUID = 20080406L;

	private String gameName;
	private String iconName;

	public CedegaGameIcon() {
		this(null, null);
	}

	public CedegaGameIcon(String gameName, String iconName) {
		this.gameName = gameName;
		this.iconName = iconName;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String name) {
		this.gameName = name;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String name) {
		this.iconName = name;
	}


	@Override
	public String toString() {
		if(gameName == null && iconName == null)
			return "Default";
		return gameName + " => " + iconName;
	}

	public boolean equals(Object o)
	{
		if(this == o) return true;
		if(o == null || getClass() != o.getClass()) return false;

		CedegaGameIcon that = (CedegaGameIcon) o;

		if(gameName != null ? !gameName.equals(that.gameName) : that.gameName != null) return false;
		if(iconName != null ? !iconName.equals(that.iconName) : that.iconName != null) return false;

		return true;
	}

	public int hashCode()
	{
		int result;
		result = (gameName != null ? gameName.hashCode() : 0);
		result = 31 * result + (iconName != null ? iconName.hashCode() : 0);
		return result;
	}
}

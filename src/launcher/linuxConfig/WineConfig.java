package launcher.linuxConfig;

/**
 * @author Oceth
 */
public class WineConfig implements LinuxConfig {
	static final long serialVersionUID = 220060612L;

	private String winePath = "/usr/bin/wine";
	private String winVer = "win2k";
	
	public String getWinePath() {
		return winePath;
	}

	public void setWinePath(String winePath) {
		this.winePath = winePath;
	}

	public String getWinVer() {
		return winVer;
	}

	public void setWinVer(String winVer) {
		this.winVer = winVer;
	}

	public String[] getWindowsEmulationModes() {
		return new String[] {"win95", "win98", "nt40", "win351", "winme", "win2k", "winxp"};
	}
	
	public WineConfig copy() {
		WineConfig ret = new WineConfig();
		ret.setWinVer(winVer);
		ret.setWinePath(winePath);
		return ret;
	}
}

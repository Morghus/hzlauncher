package launcher.linuxConfig;

import launcher.Configuration;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.HashMap;

/**
 * @author Oceth
 */
public class LinuxConfigDialog extends JDialog {
	private JSplitPane splitter;
	private JComboBox emulator;
	private WineConfigPane wineCfg;
	private CedegaConfigPane cedegaCfg;
	private Cedega52ConfigPane cedega52Cfg;

	private static Map class2Index;
	static
	{
		class2Index = new HashMap();
		class2Index.put(WineConfig.class, new Integer(0));
		class2Index.put(CedegaConfig.class, new Integer(1));
		class2Index.put(Cedega52Config.class, new Integer(2));
	}

	public LinuxConfigDialog(JDialog parent) {
		super(parent, "Emulator configuration");
		Configuration cfg = Configuration.getConfig();
		this.setModal(true);

		splitter = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

		emulator = new JComboBox(new String[]{"Wine","Cedega","Cedega >= 5.2"});
		emulator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				updateConfigView();
			}
		});
		emulator.setSelectedIndex(((Integer)class2Index.get(cfg.getLinuxConfig().getClass())).intValue());
		splitter.setTopComponent(emulator);

		this.getContentPane().add(splitter);

		updateConfigView();
	}

	public void updateConfigView() {
		switch(emulator.getSelectedIndex())
		{
			case 1: splitter.setBottomComponent(new JScrollPane(getCedegaCfg())); break;//Cedega
			case 2: splitter.setBottomComponent(new JScrollPane(getCedega52Cfg())); break;//Cedega >= 5.2
			default: splitter.setBottomComponent(new JScrollPane(getWineCfg())); //Wine 
		}
	}

	private WineConfigPane getWineCfg() {
		if(wineCfg == null)
			wineCfg = new WineConfigPane(this);
		return wineCfg;
	}

	private CedegaConfigPane getCedegaCfg() {
		if(cedegaCfg == null)
			cedegaCfg = new CedegaConfigPane(this);
		return cedegaCfg;
	}

	private Cedega52ConfigPane getCedega52Cfg() {
		if(cedega52Cfg == null)
			cedega52Cfg = new Cedega52ConfigPane(this);
		return cedega52Cfg;
	}
}

package launcher.linuxConfig;

import launcher.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Oceth
 */
public class CedegaConfigPane extends JPanel {
	private JDialog cfgDlg;
	private CedegaConfig ccfg;
	private JTextField cedegaPath;
	private JTextField debugMsgs;

	public CedegaConfigPane(JDialog confDlg) {
		this.cfgDlg = confDlg;
		if(Configuration.getConfig().getLinuxConfig() instanceof CedegaConfig)
			ccfg = ((CedegaConfig)Configuration.getConfig().getLinuxConfig()).copy();
		else
			ccfg = new CedegaConfig();

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		this.setLayout(gbl);

		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		cedegaPath = new JTextField(ccfg.getCedegaPath());
		cedegaPath.setEditable(true);
		JPanel pan = new JPanel();
		pan.setBorder(BorderFactory.createTitledBorder("Path to cedega executable"));
		pan.add(cedegaPath);
		pan.setLayout(new GridLayout(1,1));
		gbl.setConstraints(pan, gbc);
		add(pan);

		JComboBox cedegaConfig = new JComboBox(ccfg.getCedegaConfigurations());
		cedegaConfig.setSelectedItem(ccfg.getCedegaConfig());
		cedegaConfig.setBorder(BorderFactory.createTitledBorder("Cedega configuration profile"));
		cedegaConfig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox) e.getSource();
				ccfg.setCedegaConfig((String)cb.getSelectedItem());
			}
		});
		gbl.setConstraints(cedegaConfig,gbc);
		add(cedegaConfig);

		JComboBox winVer = new JComboBox(ccfg.getWindowsEmulationModes());
		winVer.setSelectedItem(ccfg.getWinVer());
		winVer.setBorder(BorderFactory.createTitledBorder("Emulated windows version"));
		winVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox) e.getSource();
				ccfg.setWinVer((String)cb.getSelectedItem());
			}
		});
		gbl.setConstraints(winVer,gbc);
		add(winVer);

		JComboBox game = new JComboBox(ccfg.getGames());
		game.setSelectedItem(ccfg.getGame());
		game.setBorder(BorderFactory.createTitledBorder("Cedega game dir"));
		game.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox) e.getSource();
				ccfg.setGame((String)cb.getSelectedItem());
			}
		});
		gbl.setConstraints(game,gbc);
		add(game);


		debugMsgs = new JTextField(ccfg.getDebugMsgs());
		debugMsgs.setEditable(true);
		pan = new JPanel();
		pan.setBorder(BorderFactory.createTitledBorder("Cedega debug string (empty to disable)"));
		pan.add(debugMsgs);
		pan.setLayout(new GridLayout(1,1));
		gbl.setConstraints(pan, gbc);
		add(pan);

		gbc.fill = GridBagConstraints.NONE;
		gbc.insets = new Insets(2,2,2,2);
		gbc.weightx = 1;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		JButton okb = new JButton("Ok");
		okb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ccfg.setCedegaPath(cedegaPath.getText());
				ccfg.setDebugMsgs(debugMsgs.getText());
				Configuration.getConfig().setLinuxConfig(ccfg);
				cfgDlg.dispose();
			}
		});
		gbl.setConstraints(okb, gbc);
		add(okb);

		gbc.anchor = GridBagConstraints.WEST;
		JButton cancelb = new JButton("Cancel");
		cancelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cfgDlg.dispose();
			}
		});
		gbl.setConstraints(cancelb, gbc);
		add(cancelb);
	}
}

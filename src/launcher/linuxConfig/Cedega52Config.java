package launcher.linuxConfig;

import org.apache.commons.configuration.INIConfiguration;
import org.apache.commons.configuration.HierarchicalINIConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.SubnodeConfiguration;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Iterator;

/**
 * @author Oceth
 */
public class Cedega52Config implements LinuxConfig {
	static final long serialVersionUID = 120060612L;

	private String cedegaPath = "/usr/bin/cedega";
	private String gddbConfig = "Default";
	private CedegaGameIcon game = new CedegaGameIcon();
	private String winVer = "Default";
    private boolean writeIniFile = false;
	private String debugMsgs = "";

	public String getCedegaPath() {
		return cedegaPath;
	}

	public void setCedegaPath(String cedegaPath) {
		this.cedegaPath = cedegaPath;
	}

	public String getGddbConfig() {
		return gddbConfig;
	}

	public void setGddbConfig(String gddbConfig) {
		this.gddbConfig = gddbConfig;
	}

	public CedegaGameIcon getGame() {
		return game;
	}

	public void setGame(CedegaGameIcon game) {
		this.game = game;
	}

	public String getWinVer() {
		return winVer;
	}

	public void setWinVer(String winVer) {
		this.winVer = winVer;
	}

    public boolean isWriteIniFile() {
        return writeIniFile;
    }

    public void setWriteIniFile(boolean writeIniFile) {
        this.writeIniFile = writeIniFile;
    }

    public String getDebugMsgs() {
		return debugMsgs;
	}

	public void setDebugMsgs(String debugMsgs) {
		this.debugMsgs = debugMsgs;
	}

	public static String[] getWindowsEmulationModes() {
		return new String[] {"Default", "win95", "win98", "nt40", "win351", "winme", "win2k", "winxp"};
	}

	public static String[] getGDDBConfigurations() {
		File cfgProf = new File(System.getProperty("user.home") + File.separator + ".cedega" + File.separator +".gddb");

		List profiles = new ArrayList();
		profiles.add("Default");

		if(cfgProf.exists() && cfgProf.isDirectory())
		{
			File[] files = cfgProf.listFiles();
			for(int i=0; i < files.length; ++i)
			{
				if(files[i].isFile() && !files[i].isHidden())
				{
					profiles.add(files[i].getName().replaceAll("\\.gddb$",""));
				}
			}
		}

		return (String[]) profiles.toArray(new String[profiles.size()]);
	}

	public static CedegaGameIcon[] getGames() {
		File cfgProf = new File(System.getProperty("user.home"),".cedega");

		List profiles = new ArrayList();
		profiles.add(new CedegaGameIcon());

		if(cfgProf.exists() && cfgProf.isDirectory())
		{
			File[] files = cfgProf.listFiles();
			for(int i=0; i < files.length; ++i)
			{
				File iniFile = new File(files[i].getPath(),"games.ini");
				if(files[i].isDirectory() && iniFile.exists() && iniFile.isFile())
				{
                    try {
                        HierarchicalINIConfiguration cfg = new HierarchicalINIConfiguration(iniFile);
                        Set<String> icons = cfg.getSections();
                        for (String icon : icons) {
                            profiles.add(new CedegaGameIcon(files[i].getName(), icon));
                        }
                    } catch (ConfigurationException e) {
                        e.printStackTrace();
                    }
				}
			}
		}

		return (CedegaGameIcon[]) profiles.toArray(new CedegaGameIcon[profiles.size()]);
	}

	public Cedega52Config copy() {
		Cedega52Config ret = new Cedega52Config();
		ret.setCedegaPath(cedegaPath);
		ret.setGddbConfig(gddbConfig);
		ret.setGame(game);
		ret.setWinVer(winVer);
        ret.setWriteIniFile(writeIniFile);
		ret.setDebugMsgs(debugMsgs);
		return ret;
	}

}

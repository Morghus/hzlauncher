package launcher.linuxConfig;

import launcher.Configuration;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Oceth
 */
public class WineConfigPane extends JPanel {
	private JDialog cfgDlg;
	private WineConfig wcfg;
	private JTextField winePath;

	public WineConfigPane(JDialog confDlg) {
		this.cfgDlg = confDlg;
		if(Configuration.getConfig().getLinuxConfig() instanceof WineConfig)
		{
			wcfg = ((WineConfig)Configuration.getConfig().getLinuxConfig()).copy();
		}
		else
		{
			wcfg = new WineConfig();
		}

		GridBagLayout gbl = new GridBagLayout();
		GridBagConstraints gbc = new GridBagConstraints();
		this.setLayout(gbl);

		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		winePath = new JTextField(wcfg.getWinePath());
		winePath.setEditable(true);
		JPanel pan = new JPanel();
		pan.setBorder(BorderFactory.createTitledBorder("Path to wine executable"));
		pan.add(winePath);
		pan.setLayout(new GridLayout(1,1));
		gbl.setConstraints(pan, gbc);
		add(pan);
		
		/* not changeable on cmdline anymore                          
		JComboBox winVer = new JComboBox(wcfg.getWindowsEmulationModes());
		winVer.setSelectedItem(wcfg.getWinVer());
		winVer.setBorder(BorderFactory.createTitledBorder("Emulated windows version"));
		winVer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox) e.getSource();
				wcfg.setWinVer((String)cb.getSelectedItem());
			}
		});
		gbl.setConstraints(winVer,gbc);
		add(winVer);
		*/
		
		gbc.fill = GridBagConstraints.NONE;
		gbc.insets = new Insets(2,2,2,2);
		gbc.weightx = 1;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.gridwidth = GridBagConstraints.RELATIVE;
		JButton okb = new JButton("Ok");
		okb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wcfg.setWinePath(winePath.getText());
				Configuration.getConfig().setLinuxConfig(wcfg);
				cfgDlg.dispose();
			}
		});
		gbl.setConstraints(okb, gbc);
		add(okb);
		
		gbc.anchor = GridBagConstraints.WEST;
		JButton cancelb = new JButton("Cancel");
		cancelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cfgDlg.dispose();
			}
		});
		gbl.setConstraints(cancelb, gbc);
		add(cancelb);
	}
}

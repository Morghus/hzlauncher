package launcher;
import launcher.linuxConfig.LinuxConfig;
import launcher.linuxConfig.WineConfig;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.beans.XMLEncoder;
import java.beans.XMLDecoder;

/*
 * Created on 20.05.2004
 * $Id: Configuration.java,v 1.5 2006/06/14 12:06:17 oceth Exp $
 */
/**
 * @author unknown
 */
public class Configuration implements Serializable {
	
	private static final long serialVersionUID = 20080714L;
	
	private String horizonsPath;
	private HZCharacter[] hzCharacters;
	private HZCharacter lastChar;
	private HZAccount hzAccount;
	private boolean performCRCcheck = false;
	private boolean disableGetFileSizes = true;
	private boolean keepNewerFiles = false;
	private String patchURL = "";
	private String websiteURL = "";
	private String sessionId = "";
	private Date sessionIdTime = null;
	private LinuxConfig linuxConfig = null;
	private boolean patchShaderFiles = false;
	private boolean enableDebugConsole = false;
	private boolean enableVersionCheck = true;
    private boolean useLegacyServiceImpl = true;

	private static Configuration config = null;
	
	public static Configuration getConfig() {
		if (config == null)
			loadConfig();
		return config;
	}

	private static String getFileName()
	{
		if(HorizonsLauncherStrategy.Factory.isLinux())
			return ".hzlaunchercfg.xml";
		return "HZLauncherConfiguration.xml";
	}
	
	public static void loadConfig() {
		try {
			XMLDecoder in = new XMLDecoder(new FileInputStream(System.getProperty("user.home") + File.separator + getFileName()));
			config = (Configuration) in.readObject();
		}
		catch(Exception e)
		{
			// if xml load failed, try legacy load
			ObjectInputStream in = null;
			try {
				in = new ObjectInputStream(new FileInputStream(System.getProperty("user.home") + File.separator + "HZLauncherConfiguration.dat"));
				config = (Configuration)in.readObject();
			} catch (Exception e2) {}
		}
		if (config == null)
			config = new Configuration();
	}
	
	public static void saveConfig() {
		try {
			XMLEncoder out = new XMLEncoder(new FileOutputStream(System.getProperty("user.home") + File.separator + getFileName()));
			out.writeObject(config);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Configuration() {
	}
	
	public void setSessionId(String id) {
		sessionId = id;
		if (id == null)
			sessionIdTime = null;
		else
			sessionIdTime = new Date();			
	}
	
	public void touchSessionIdTime() {
		sessionIdTime = new Date();
	}
	
	public String getSessionId() {
		return sessionId;
	}
	
	public Date getSessionIdTime() {
		return sessionIdTime;
	}

	
	/**
	 * @return Returns the horizonsPath.
	 */
	public String getHorizonsPath() {
		return horizonsPath;
	}
	/**
	 * @param horizonsPath The horizonsPath to set.
	 */
	public void setHorizonsPath(String horizonsPath) {
		this.horizonsPath = horizonsPath;
	}

	public void setPatchURL(String url) {
		this.patchURL = url;
	}
	
	public String getPatchURL() {
		// always return US URL - There is no EU Server anymore
		return "patch.istaria.com";
	}
	/**
	 * @return Returns the lastChar.
	 */
	public HZCharacter getLastChar() {
		return lastChar;
	}
	/**
	 * @param lastChar The lastChar to set.
	 */
	public void setLastChar(HZCharacter lastChar) {
		this.lastChar = lastChar;
	}
	/**
	 * @return Returns the performCRCcheck.
	 */
	public boolean isPerformCRCcheck() {
		return performCRCcheck;
	}
	/**
	 * @param performCRCcheck The performCRCcheck to set.
	 */
	public void setPerformCRCcheck(boolean performCRCcheck) {
		this.performCRCcheck = performCRCcheck;
	}
	/**
	 * @return Returns the hzAccount.
	 */
	public HZAccount getHzAccount() {
		return hzAccount;
	}
	/**
	 * @param hzAccount The hzAccount to set.
	 */
	public void setHzAccount(HZAccount hzAccount) {
		this.hzAccount = hzAccount;
	}
	/**
	 * @return Returns the hzCharacters.
	 */
	public HZCharacter[] getHzCharacters() {
		return hzCharacters;
	}
	/**
	 * @param hzCharacters The hzCharacters to set.
	 */
	public void setHzCharacters(HZCharacter[] hzCharacters) {
		this.hzCharacters = hzCharacters;
	}
	/**
	 * @return Returns the websiteURL.
	 */
	public String getWebsiteURL() {
		// always return US URL - There is no EU Server anymore
		return "play.istaria.com";
	}
	/**
	 * @param websiteURL The websiteURL to set.
	 */
	public void setWebsiteURL(String websiteURL) {
		this.websiteURL = websiteURL;
	}

	/**
	 * @return
	 */
	public String[] getCharacterShards() {
		HZCharacter[] chars = getHzCharacters();
		LinkedList shardList = new LinkedList();

		if(chars != null)
		{
			for (int i = 0; i < chars.length; i++) {
				Iterator it = shardList.iterator();
				boolean found = false;
				while (it.hasNext()) {
					if (((String)it.next()).equals(chars[i].getShardName())) {
						found = true;
						break;
					}
				}
				if (!found)
					shardList.add(chars[i].getShardName());
			}
		}
		
		if (shardList.size() == 0)
			return null;
		
		String[] ret = new String[shardList.size()];
		Iterator it = shardList.iterator();
		for (int i = 0; it.hasNext(); i++)
			ret[i] = (String)it.next();
		
		return ret;
	}
	/**
	 * @return Returns the disableGetFileSizes.
	 */
	public boolean isDisableGetFileSizes() {
		return disableGetFileSizes;
	}
	/**
	 * @param disableGetFileSizes The disableGetFileSizes to set.
	 */
	public void setDisableGetFileSizes(boolean disableGetFileSizes) {
		this.disableGetFileSizes = disableGetFileSizes;
	}
	/**
	 * @return Returns the keepNewerFiles.
	 */
	public boolean isKeepNewerFiles() {
		return keepNewerFiles;
	}
	/**
	 * @param keepNewerFiles The keepNewerFiles to set.
	 */
	public void setKeepNewerFiles(boolean keepNewerFiles) {
		this.keepNewerFiles = keepNewerFiles;
	}

	/**
 	 * @return Returns the linux configuration
	 */
	public LinuxConfig getLinuxConfig() {
		if(this.linuxConfig == null)
			return this.linuxConfig = new WineConfig();
		return this.linuxConfig;
	}

	/**
	 * @param linuxConfig The linux configuration to set.
	 */
	public void setLinuxConfig(LinuxConfig linuxConfig) {
		this.linuxConfig = linuxConfig;
	}

	/**
	 * @return Returns true if shader files should be patched
	 */
	public boolean isPatchShaderFiles() {
		return patchShaderFiles;
	}

	/**
	 * @param patchShaderFiles sets patchShaderFiles
	 */
	public void setPatchShaderFiles(boolean patchShaderFiles) {
		this.patchShaderFiles = patchShaderFiles;
	}

	public boolean isEnableDebugConsole()
	{
		return enableDebugConsole;
	}

	public void setEnableDebugConsole(boolean enableDebugConsole)
	{
		this.enableDebugConsole = enableDebugConsole;
	}


	public boolean isEnableVersionCheck() {
		return enableVersionCheck;
	}

	public void setEnableVersionCheck(boolean enableVersionCheck) {
		this.enableVersionCheck = enableVersionCheck;
	}

    public boolean isUseLegacyServiceImpl() {
        return useLegacyServiceImpl;
    }

    public void setUseLegacyServiceImpl(boolean useLegacyServiceImpl) {
        this.useLegacyServiceImpl = useLegacyServiceImpl;
    }

    public static File detectExecutable() {
        File istariaExe = null;
        File horizonsExe = null;
        File path = new File(Configuration.getConfig().getHorizonsPath());
        File[] files = path.listFiles();
        for (File file : files) {
            if(file.isFile()) {
                if(file.getName().equalsIgnoreCase("horizons.exe"))
                    horizonsExe = file;
                else if(file.getName().equalsIgnoreCase("istaria.exe"))
                    istariaExe = file;
            }
        }
        if(istariaExe != null)
            return istariaExe;
        return horizonsExe;
    }
}
